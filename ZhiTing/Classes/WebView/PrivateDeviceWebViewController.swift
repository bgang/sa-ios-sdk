//
//  PrivateDeviceWebViewController.swift
//  ZhiTing
//
//  Created by iMac on 2022/5/18.
//

import Foundation
import UIKit
import WebKit

class PrivateDeviceWebViewController: WKWebViewController {
    var privateDevice: Device?
    var area = AuthManager.shared.currentArea
    // 初始url
    var originLink: String?
    
    init(link: String, privateDevice: Device? = nil) {
        self.privateDevice = privateDevice
        /// 处理编码问题
        let encodedLink = link.urlDecoded()
            .urlEncoded()

        self.originLink = link

        super.init(link: encodedLink)
        doorLockUtil = DoorLockUtil()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    
    private lazy var progress: UIProgressView = {
        let progres = UIProgressView.init(progressViewStyle: .default)
        progres.frame = CGRect(x: 0, y: 0, width: Screen.screenWidth, height: 1.5)
        progres.progress = 0
        progres.progressTintColor = .custom(.blue_2da3f6)
        progres.trackTintColor = UIColor.clear
        return progres
    }()
    
    private lazy var settingButton = Button().then {
        $0.setImage(.assets(.settings), for: .normal)
        $0.frame.size = CGSize(width: 18, height: 18)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingButton.clickCallBack = { [weak self] _ in
            guard let self = self else { return }
            let vc = PrivateDeviceDetailViewController()
            vc.device = self.privateDevice
            vc.area = self.area
            vc.renameCallback = { [weak self] name in
                guard let self = self else { return }
                self.doorLockUtil?.doorLock?.name = name
                /// 处理编码问题
                self.originLink = self.originLink?.replacingOccurrences(of: "&is_first=1", with: "")
                
                if let encodedLink = self.originLink?.urlDecoded().urlEncoded().replacingOccurrences(of: "%23", with: "#"),
                   let linkURL = URL(string: encodedLink) {
                    self.webView.load(URLRequest(url: linkURL))
                    self.webView.backForwardList.perform(Selector(("_removeAllItems")))
                }
                self.webView.reload()
                
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.settingButton)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        super.webView(webView, didFinish: navigation)
    }
   
}
