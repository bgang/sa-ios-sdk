//
//  EditMailSelectedView.swift
//  ZhiTing
//
//  Created by zy on 2022/6/9.
//

import UIKit

class EditMailSelectedView: UIView {

    var isChooesPhone: Bool?
    
    private lazy var coverView = UIView().then {
        $0.backgroundColor = .custom(.white_ffffff)
    }
    
    private lazy var phoneIcon = ImageView().then {
        $0.image = .assets(.mail_phoneIcon)
    }
    
    private lazy var phoneTitle = Label().then {
        $0.font = .font(size: 14, type: .bold)
        $0.textColor = .custom(.black_3f4663)
        $0.textAlignment = .left
        $0.text = "使用手机验证".localizedString
    }
    
    lazy var phoneDesLabel = Label().then {
        $0.font = .font(size: 14, type: .medium)
        $0.textColor = .custom(.black_3f4663)
        $0.textAlignment = .left
        $0.text = "当前手机".localizedString + UserManager.shared.currentUser.phone
    }
    
    private lazy var phoneSelectBtn = Button().then {
        $0.setImage(.assets(.unselected_tick), for: .normal)
        $0.setImage(.assets(.selected_tick), for: .selected)
        $0.isSelected = false
        $0.addTarget(self, action: #selector(phoneSelectedAction), for: .touchUpInside)
    }
    
    private lazy var mailIcon = ImageView().then {
        $0.image = .assets(.mail_mailIcon)
    }
    
    private lazy var mailTitle = Label().then {
        $0.font = .font(size: 14, type: .bold)
        $0.textColor = .custom(.black_3f4663)
        $0.textAlignment = .left
        $0.text = "使用邮箱验证".localizedString
    }
    
    lazy var mailDesLabel = Label().then {
        $0.font = .font(size: 14, type: .medium)
        $0.textColor = .custom(.black_3f4663)
        $0.textAlignment = .left
        $0.text = "当前邮箱".localizedString + UserManager.shared.currentUser.email
    }
    
    private lazy var mailSelectBtn = Button().then {
        $0.setImage(.assets(.unselected_tick), for: .normal)
        $0.setImage(.assets(.selected_tick), for: .selected)
        $0.isSelected = false
        $0.addTarget(self, action: #selector(mailSelectedAction), for: .touchUpInside)
    }


    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews(){
        addSubview(coverView)
        coverView.addSubview(phoneIcon)
        coverView.addSubview(phoneTitle)
        coverView.addSubview(phoneDesLabel)
        coverView.addSubview(phoneSelectBtn)
        
        coverView.addSubview(mailIcon)
        coverView.addSubview(mailTitle)
        coverView.addSubview(mailDesLabel)
        coverView.addSubview(mailSelectBtn)
        
    }
    
    private func setupConstraints(){
        
        coverView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        phoneIcon.snp.makeConstraints {
            $0.top.equalTo(ZTScaleValue(20))
            $0.left.equalToSuperview()
            $0.width.height.equalTo(ZTScaleValue(40))
        }
        
        phoneTitle.snp.makeConstraints {
            $0.left.equalTo(phoneIcon.snp.right).offset(ZTScaleValue(10))
            $0.top.equalTo(phoneIcon)
            $0.width.greaterThanOrEqualTo(ZTScaleValue(80))
        }
        
        phoneDesLabel.snp.makeConstraints {
            $0.left.equalTo(phoneIcon.snp.right).offset(ZTScaleValue(10))
            $0.bottom.equalTo(phoneIcon)
            $0.width.greaterThanOrEqualTo(ZTScaleValue(80))
        }
        
        phoneSelectBtn.snp.makeConstraints {
            $0.centerY.equalTo(phoneIcon)
            $0.right.equalToSuperview()
            $0.width.height.equalTo(ZTScaleValue(18.5))
        }
        
        
        mailIcon.snp.makeConstraints {
            $0.top.equalTo(phoneIcon.snp.bottom).offset(ZTScaleValue(40))
            $0.left.equalToSuperview()
            $0.width.height.equalTo(ZTScaleValue(40))
            $0.bottom.equalTo(-ZTScaleValue(20))
        }
        
        mailTitle.snp.makeConstraints {
            $0.left.equalTo(mailIcon.snp.right).offset(ZTScaleValue(10))
            $0.top.equalTo(mailIcon)
            $0.width.greaterThanOrEqualTo(ZTScaleValue(80))
        }
        
        mailDesLabel.snp.makeConstraints {
            $0.left.equalTo(mailIcon.snp.right).offset(ZTScaleValue(10))
            $0.bottom.equalTo(mailIcon)
            $0.width.greaterThanOrEqualTo(ZTScaleValue(80))
        }
        
        mailSelectBtn.snp.makeConstraints {
            $0.centerY.equalTo(mailIcon)
            $0.right.equalToSuperview()
            $0.width.height.equalTo(ZTScaleValue(18.5))
        }
        
    }
    
    @objc
    private func phoneSelectedAction(){
        phoneSelectBtn.isSelected = true
        mailSelectBtn.isSelected = false
        isChooesPhone = true
    }
    
    @objc
    private func mailSelectedAction(){
        phoneSelectBtn.isSelected = false
        mailSelectBtn.isSelected = true
        isChooesPhone = false
    }
    
}
