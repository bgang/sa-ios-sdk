//
//  BrandMainViewController.swift
//  ZhiTing
//
//  Created by iMac on 2021/2/24.
//

import UIKit
import Alamofire
import JXSegmentedView

class BrandMainViewController: BaseViewController {
    private lazy var brands = [Brand]()

    private lazy var emptyView = EmptyStyleView(frame: .zero, style: .noContent)
    
    private lazy var tableView = UITableView(frame: .zero, style: .plain).then {
        $0.separatorStyle = .none
        $0.backgroundColor = .custom(.white_ffffff)
        $0.register(BrandCell.self, forCellReuseIdentifier: BrandCell.reusableIdentifier)
        $0.rowHeight = UITableView.automaticDimension
        $0.estimatedRowHeight = 80
        $0.delegate = self
        $0.dataSource = self
    }
    
    private lazy var myPluginButton = Button().then {
        $0.setTitle("我的插件".localizedString, for: .normal)
        $0.titleLabel?.font = .font(size: ZTScaleValue(14), type: .bold)
        $0.setTitleColor(.custom(.black_3f4663), for: .normal)
        $0.clickCallBack = { [weak self] _ in
            guard let self = self else { return }
            let vc = PluginMainViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private lazy var searchButton = Button().then {
        $0.layer.cornerRadius = 16
        $0.layer.masksToBounds = true
        $0.backgroundColor = .custom(.gray_f6f8fd)
        $0.setTitle("搜索".localizedString, for: .normal)
        $0.setTitleColor(.custom(.gray_94a5be), for: .normal)
        $0.setImage(UIImage.assets(.search), for: .normal)
        $0.titleLabel?.font = .font(size: 14, type: .medium)
        $0.titleEdgeInsets = UIEdgeInsets(top: 0, left: ZTScaleValue(-255), bottom: 0, right: 0)
        $0.imageEdgeInsets = UIEdgeInsets(top: 0, left: ZTScaleValue(-275), bottom: 0, right: 0)
        $0.clickCallBack = { [weak self] _ in
            guard let self = self else { return }
            let brandSystemSearchVC = BrandSearchViewController()
            brandSystemSearchVC.selectCallback = { [weak self] name in
                guard let self = self else { return }
                let brandDetailVC = BrandDetailViewController()
                brandDetailVC.brand_name = name
                self.navigationController?.pushViewController(brandDetailVC, animated: true)
            }
            brandSystemSearchVC.modalPresentationStyle = .fullScreen
            self.present(brandSystemSearchVC, animated: true)
        }
    }
    

    private lazy var headerView = BrandListHeader(text: "系统已支持以下品牌的设备".localizedString)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        requestNetwork()
        navigationItem.title = "支持品牌".localizedString
        if authManager.isSAEnviroment {
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: myPluginButton)
        }
    }
    
    override func setupViews() {
        view.backgroundColor = .custom(.white_ffffff)
        
        view.addSubview(searchButton)
        view.addSubview(headerView)
        view.addSubview(tableView)
        
        let header = ZTGIFRefreshHeader()
        tableView.mj_header = header
        tableView.mj_header?.setRefreshingTarget(self, refreshingAction: #selector(requestNetwork))
        
        tableView.addSubview(emptyView)
        emptyView.isHidden = true
    }
    
    override func setupConstraints() {
        searchButton.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Screen.k_nav_height + ZTScaleValue(14))
            $0.height.equalTo(ZTScaleValue(32));
            $0.left.equalToSuperview().offset(ZTScaleValue(13.5))
            $0.right.equalToSuperview().offset(ZTScaleValue(-13.5))
        }
        
        headerView.snp.makeConstraints {
            $0.top.equalTo(searchButton.snp.bottom)
            $0.left.right.equalToSuperview()
            $0.height.equalTo(ZTScaleValue(39))
        }

        tableView.snp.makeConstraints {
            $0.top.equalTo(headerView.snp.bottom)
            $0.left.right.bottom.equalToSuperview()
        }
        
        emptyView.snp.makeConstraints {
            $0.width.equalTo(tableView)
            $0.height.equalTo(tableView)
            $0.center.equalToSuperview()
        }
        
    }
    
    @objc func requestNetwork() {
        guard authManager.isSAEnviroment else { return }
        ApiServiceManager.shared.brands(name: "") { [weak self] (response) in
            guard let self = self else { return }
            self.brands = response.brands
            self.emptyView.isHidden = self.brands.count != 0
            self.tableView.reloadData()
            

            self.tableView.mj_header?.endRefreshing()
            
        } failureCallback: { [weak self] (code, err) in
            guard let self = self else { return }
            self.emptyView.isHidden = self.brands.count != 0
            self.tableView.mj_header?.endRefreshing()
            self.showToast(string: err)
        }

    }
    
}

extension BrandMainViewController {
    
    func installPlugin(brand: Brand) {
        let plugins = brand.plugins.filter({ !$0.is_newest || !$0.is_added }).map(\.id)
        brand.is_updating = true
        tableView.reloadData()
        ApiServiceManager.shared.installPlugin(name: brand.name, plugins: plugins) { [weak self] resp in
            guard let self = self else { return }
            brand.is_updating = false
            if resp.success_plugins.count >= brand.plugins.count {
                brand.is_newest = true
                brand.is_added = true
            }
            
            self.tableView.reloadData()
        } failureCallback: { [weak self] code, err in
            guard let self = self else { return }
            self.showToast(string: err)
            brand.is_updating = false
            self.tableView.reloadData()
        }
    }

}


extension BrandMainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return brands.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BrandCell.reusableIdentifier, for: indexPath) as! BrandCell
        let brand = brands[indexPath.row]
        cell.brand = brand
        cell.buttonCallback = { [weak self] in
            guard let self = self else { return }
            self.installPlugin(brand: brand)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = BrandDetailViewController()
        vc.brand_name = brands[indexPath.row].name
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

extension BrandMainViewController: JXSegmentedListContainerViewListDelegate {
    func listView() -> UIView {
        return view
    }
}
