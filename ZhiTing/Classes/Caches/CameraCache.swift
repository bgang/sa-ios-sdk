//
//  CameraCache.swift
//  ZhiTing
//
//  Created by macbook on 2022/5/9.
//

import UIKit
import RealmSwift

class CameraCache: Object {
    //设备ID
    @Persisted var uid: String?
    //设备密码
    @Persisted var pwd = ""
    //设备名称
    @Persisted var name = ""
    //设备tmpdid
    @Persisted var tmpdid: String?
    //设备lastConnetTime
    @Persisted var lastTime: Int?

    /// 更新摄像机信息
    /// - Parameter camera: 摄像机数据
    static func update(from camera: CameraModel) {
        //创建一个Realm对象
        let realm = try! Realm()
        
        if let cameraCache = realm.objects(CameraCache.self).first {
            try? realm.write {
                if camera.uid != "" {
                    cameraCache.uid = camera.uid
                }
                cameraCache.pwd = camera.pwd
                cameraCache.name = camera.name
                if camera.tmpdid != "" {
                    cameraCache.tmpdid = camera.tmpdid
                }
            }
        } else {
            let cameraCache = CameraCache()
            if camera.uid != "" {
                cameraCache.uid = camera.uid
            }
            cameraCache.pwd = camera.pwd
            cameraCache.name = camera.name
            cameraCache.lastTime = 0
            if camera.tmpdid != "" {
                cameraCache.tmpdid = camera.tmpdid
            }
            try? realm.write {
                realm.add(cameraCache)
            }
        }
    }
    
    static func updateVUIDLastConnetTime(did: String, tmpDid: String?, time: Int?){
        let realm = try! Realm()
        let result = realm.objects(CameraCache.self).filter("uid = \"\(did)\"")
        let camera = result.first
        try? realm.write {
            if tmpDid != nil {
                camera?.tmpdid = tmpDid
            }
            if time != nil{
                camera?.lastTime = time
            }
        }

    }
    
    static func getCameras() -> [CameraModel] {
        let realm = try! Realm()
        var cameras = [CameraModel]()
        let cameraCaches = realm.objects(CameraCache.self)
        cameraCaches.forEach {
            let camera = CameraModel()
            camera.uid = $0.uid
            camera.pwd = $0.pwd
            camera.name = $0.name
            camera.tmpdid = $0.tmpdid
            camera.lastTime = $0.lastTime
            cameras.append(camera)
        }
        return cameras
    }
    
    static func getCamera(uid: String) -> CameraModel {
        let realm = try! Realm()
        let model = CameraModel()
        if let result = realm.objects(CameraCache.self).filter("uid = \"\(uid)\"").first {
            model.uid = result.uid
            model.tmpdid = result.tmpdid
            model.pwd = result.pwd
            model.name = result.name
        }
        return model
    }

}

class CommonPlaceCache: Object {
    //设备ID
    @Persisted var uid: String?
    //常用位置ID
    @Persisted var commonPlace = 0
    //常用位置图片
    @Persisted var commonPlaceData = Data()
    //预设位置名称
    @Persisted var placeName: String
    
    static func cacheCommonPlace(uid: String?, commonPlace: Int, placeName: String , commonPlaceData: Data?) {
        let realm = try! Realm()
        let commonCache = CommonPlaceCache()
        commonCache.uid = uid
        commonCache.commonPlace = commonPlace
        commonCache.commonPlaceData = commonPlaceData ?? Data()
        commonCache.placeName = placeName
        
        try? realm.write {
            realm.add(commonCache)
        }
    }
    
    static func getCameraCommonPlaces(uid: String) -> [CameraCommonPlaceModel] {
        let realm = try! Realm()
        var commonPlaces = [CameraCommonPlaceModel]()
        //筛选相应ID的摄像机的预设位置
        let result = realm.objects(CommonPlaceCache.self).filter("uid = \"\(uid)\"")
        result.forEach {
            let commonPlaceModel = CameraCommonPlaceModel()
            commonPlaceModel.commonPlace = $0.commonPlace
            commonPlaceModel.commonPlaceData = $0.commonPlaceData
            commonPlaceModel.placeName = $0.placeName
            commonPlaces.append(commonPlaceModel)
        }
        return commonPlaces
    }
    
    static func deleteCameraCommonPlaces(places: [CameraCommonPlaceModel],uid: String) {
        let realm = try! Realm()
        let _uid = uid == "nil" ? "nil" : "\"\(uid)\""
        places.forEach { place in
            if let result = realm.objects(CommonPlaceCache.self).filter("uid = \(_uid) AND commonPlace = \(place.commonPlace)").first {
                try? realm.write {
                    realm.delete(result)
                }
            }
        }

    }
    
}


class CameraModel: BaseModel {
    //设备ID
     var uid: String?
    //设备密码
     var pwd = ""
    //设备名称
     var name = ""
    //设备tmpdid
     var tmpdid: String?
    //设备lastConnetTime
     var lastTime: Int?
    //设备型号
     var model = ""
    //厂商
     var manufacturer = ""
}

class CameraCommonPlaceModel: BaseModel {

    //常用位置ID
    var commonPlace = 0
    //常用位置图片
    var commonPlaceData = Data()
    //预设位置名称
    var placeName = ""
    //是否已选择
    var isSelected = false
}
