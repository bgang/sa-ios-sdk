//
//  DoorLockCache.swift
//  ZhiTing
//
//  Created by iMac on 2022/5/10.
//

import Foundation
import RealmSwift

/// 门锁设备
class DoorLockCache: Object {
    /// 所属家庭id
    @Persisted var area_id: String?
    /// 蓝牙设备名称
    @Persisted var name = ""
    /// 蓝牙设备model
    @Persisted var model = ""
    /// 设备唯一标识
    @Persisted var uuid = ""
    /// 用于数据加密的设备PIN码
    @Persisted var pin: Data?
    /// 通信数据是否需要PIN码加解密
    @Persisted var needEncode = true
    /// 蓝牙mac地址
    @Persisted var mac_addr: Data?
    /// 插件包id
    @Persisted var plugin_id: String?
}

/// 门锁用户
class DoorLockUserCache: Object {
    /// 对应锁uuid
    @Persisted var lock_id = ""
    /// 用户id
    @Persisted var id = 0
    /// 用户名
    @Persisted var name = ""
    /// 用户类型 1:普通用户 2:一次性 3:常访客 4:胁迫 6:管理员
    @Persisted var role_type = 1
    
}

/// 验证方式
class DoorLockVerificationCache: Object {
    /// 对应锁uuid
    @Persisted var lock_id = ""
    /// 门锁本地编号
    @Persisted var number_id = 0
    /// 验证方式类型 1:密码 2:指纹 6: nfc
    @Persisted var number_type = 1
    /// 名称
    @Persisted var number_name = ""
    /// 时段信息
    @Persisted var valid_time: String?
    /// 用户类型 1:普通用户 2:一次性 3:常访客 4:胁迫 6:管理员
    @Persisted var role_type = 0
    /// 创建时间
    @Persisted var create_time: Int?
    /// 绑定的用户的id 空则为未绑定的验证方式
    @Persisted var user_id: Int?
    /// 密码(只存一次性密码)
    @Persisted var password: String?
}

/// 日志
class DoorLockLogCache: Object {
    /// 对应锁uuid
    @Persisted var lock_id = ""
    /// 日志id
    @Persisted var id = 0
    /// 时间
    @Persisted var time = 0
    ///    1 开门成功
    ///    2 指纹或密码开锁连续失败5次
    ///    3 有人按门铃
    ///    4 注册用户上报
    ///    5 删除用户上报
    ///    6 撬锁告警上报
    ///    7 紧急胁迫报警上报
    @Persisted var event_type = 0
    /// 2指纹/1密码/6存储卡 ,开门成功/添加用户/删除用户/胁迫开门时有值
    @Persisted var number_type: Int?
    /// 编号，开门成功/添加用户/删除用户/胁迫开门时有值
    @Persisted var number_id: Int?
    
    
    func toJsonStr() -> String {
        var params = [String]()
        params.append("\"id\": \(id)")
        params.append("\"event_type\": \(event_type)")
        params.append("\"time\": \(time)")
        if let number_type = number_type {
            params.append("\"number_type\": \(number_type)")
        }
        if let number_id = number_id {
            params.append("\"number_id\": \(number_id)")
        }
        return "{" + params.joined(separator: ",") + "}"
    }
}
