//
//  CloudRemotePlaybackViewController.swift
//  ZhiTing
//
//  Created by macbook on 2022/5/12.
//

import UIKit
import JXSegmentedView

class CloudRemotePlaybackViewController: WKWebViewController {
    var myUID = ""
    var mypwd = ""
    
    override init(link: String) {
        super.init(link: link)
        doorLockUtil = DoorLockUtil()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

}

extension CloudRemotePlaybackViewController: JXSegmentedListContainerViewListDelegate {
    func listView() -> UIView {
        return view
    }
}

