//
//  RemotePlaybackDownView.swift
//  ZhiTing
//
//  Created by macbook on 2022/5/12.
//

import UIKit

class RemotePlaybackDownView: UIView {
    
    var clickCallback: ((_ index: Int) -> ())?
    
    lazy var bgImgView = ImageView().then {
        $0.image = .assets(.camera_shadow_down)
        $0.isUserInteractionEnabled = true
    }
    
    lazy var fullScreenBtn = Button().then {
        $0.setImage(.assets(.camera_intoLanscape), for: .normal)
        $0.setImage(.assets(.camera_intoPortrait), for: .selected)
        $0.tag = 0
        $0.isEnhanceClick = true
        $0.addTarget(self, action: #selector(btnAction(sender:)), for: .touchUpInside)
    }
    
    lazy var voice = Button().then {
        $0.setImage(.assets(.camera_voice_on), for: .normal)
        $0.setImage(.assets(.camera_voice_off), for: .selected)
        $0.tag = 1
        $0.isEnhanceClick = true
        $0.addTarget(self, action: #selector(btnAction(sender:)), for: .touchUpInside)
    }

    lazy var slider = FDMProgressView().then {
        $0.progressHeight = 3
    }
    

    lazy var playBtn = Button().then {
        $0.setImage(.assets(.camera_playback_stop), for: .normal)
        $0.setImage(.assets(.camera_playback_play), for: .selected)
        $0.tag = 2
        $0.addTarget(self, action: #selector(btnAction(sender:)), for: .touchUpInside)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
        layoutIfNeeded()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    private func setupViews(){
        addSubview(bgImgView)
        bgImgView.addSubview(fullScreenBtn)
//        bgImgView.addSubview(voice)
        bgImgView.addSubview(slider)
        bgImgView.addSubview(playBtn)
    }
    
    private func setupConstraints(){
        bgImgView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        fullScreenBtn.snp.makeConstraints {
            $0.top.equalTo(10)
            $0.right.equalTo(-15)
            $0.bottom.equalTo(-15)
            $0.width.height.equalTo(20)
        }
        
//        voice.snp.makeConstraints {
//            $0.right.equalTo(fullScreenBtn.snp.left).offset(-15)
//            $0.centerY.equalTo(fullScreenBtn)
//            $0.width.height.equalTo(20)
//        }
        
        playBtn.snp.makeConstraints {
            $0.left.equalTo(15)
            $0.centerY.equalTo(fullScreenBtn)
            $0.width.height.equalTo(20)
        }

        slider.snp.makeConstraints {
            $0.centerY.equalTo(fullScreenBtn)
            $0.left.equalTo(playBtn.snp.right).offset(15)
            $0.right.equalTo(fullScreenBtn.snp.left).offset(-15)
            $0.height.equalTo(30)
        }

    }
    
    @objc private func btnAction(sender: Button){
        switch sender.tag {
        case 0://全屏
//            sender.isSelected = !sender.isSelected
            clickCallback?(0)
        case 1://声音
            sender.isSelected = !sender.isSelected
            clickCallback?(1)
        case 2://播放
            sender.isSelected = !sender.isSelected
            clickCallback?(2)
        default:
            break
        }
    }
    
    
}
