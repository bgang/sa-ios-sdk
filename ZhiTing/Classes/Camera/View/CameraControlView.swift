//
//  CameraControlView.swift
//  ZhiTing
//
//  Created by macbook on 2022/5/5.
//
#if !(targetEnvironment(simulator))
import UIKit

class CameraControlView: UIView {
    
    var clickCallback: ((_ index: Int) -> ())?

    lazy var triangleView = UIView().then {
        $0.backgroundColor = .custom(.white_ffffff)
        // 画三角
        let trianglePath = UIBezierPath()
        var point = CGPoint(x: 0, y: ZTScaleValue(10))
        trianglePath.move(to: point)
        point = CGPoint(x: ZTScaleValue(10), y: 0)
        trianglePath.addLine(to: point)
        point = CGPoint(x: ZTScaleValue(20), y: ZTScaleValue(10))
        trianglePath.addLine(to: point)
        trianglePath.close()
        let triangleLayer = CAShapeLayer()
        triangleLayer.path = trianglePath.cgPath
        triangleLayer.fillColor = UIColor.custom(.gray_f5f5f5).cgColor
        $0.layer.addSublayer(triangleLayer)
    }
        
    //控制列表
    private lazy var flowLayout = UICollectionViewFlowLayout().then {
        $0.itemSize = CGSize(width: ZTScaleValue(40), height:  ZTScaleValue(100))
        //行列间距
        $0.minimumLineSpacing = ZTScaleValue(10)
        $0.minimumInteritemSpacing = ZTScaleValue(10)
        $0.sectionInset = .init(top: 0, left: ZTScaleValue(20), bottom: 0, right: ZTScaleValue(20))
    }
    
    lazy var controlCollectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout).then{
        $0.backgroundColor = .custom(.gray_f5f5f5)
        $0.delegate = self
        $0.dataSource = self
        $0.showsHorizontalScrollIndicator = false
        $0.showsVerticalScrollIndicator = false
        $0.alwaysBounceVertical = true
        $0.alwaysBounceHorizontal = false
        $0.register(CameraControlCell.self, forCellWithReuseIdentifier: CameraControlCell.reusableIdentifier)
    }

    var cameraControlDatas = [CameraFuctionModel]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
        setupControlDatas()
        layoutIfNeeded()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    private func setupViews(){
        addSubview(triangleView)
        addSubview(controlCollectionView)
    }
    
    private func setupConstraints(){
        triangleView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.right.equalTo(-ZTScaleValue(37.5))
            $0.width.equalTo(ZTScaleValue(20))
            $0.height.equalTo(ZTScaleValue(10))
        }
        controlCollectionView.snp.makeConstraints {
            $0.top.equalTo(triangleView.snp.bottom)
            $0.left.right.bottom.equalToSuperview()
        }

    }
    
    private func setupControlDatas(){
        let cameraLeftRight = CameraFuctionModel()
        cameraLeftRight.normalIcon = .assets(.camera_leftRight)
        cameraLeftRight.selectedIcon = .assets(.camera_leftRight)
        cameraLeftRight.title = "左右巡航"
        
        let cameraUpDown = CameraFuctionModel()
        cameraUpDown.normalIcon = .assets(.camera_upDown)
        cameraUpDown.selectedIcon = .assets(.camera_upDown)
        cameraUpDown.title = "上下巡航"
        
        let cameraPlayback = CameraFuctionModel()
        cameraPlayback.normalIcon = .assets(.camera_playback)
        cameraPlayback.selectedIcon = .assets(.camera_playback)
        cameraPlayback.title = "回放"
        
        let cameraLock = CameraFuctionModel()
        cameraLock.normalIcon = .assets(.camera_lock)
        cameraLock.selectedIcon = .assets(.camera_lock_selected)
        cameraLock.title = "锁定画面"
        
        let cameraNormalPlace = CameraFuctionModel()
        cameraNormalPlace.normalIcon = .assets(.camera_normalPlace)
        cameraNormalPlace.selectedIcon = .assets(.camera_normalPlace)
        cameraNormalPlace.title = "常看位置"
        
        let cameraCollection = CameraFuctionModel()
        cameraCollection.normalIcon = .assets(.camera_collection)
        cameraCollection.selectedIcon = .assets(.camera_collection)
        cameraCollection.title = "收藏位置"
        
        cameraControlDatas = [cameraLeftRight, cameraUpDown, cameraPlayback, cameraLock, cameraNormalPlace, cameraCollection]
        //刷新数据
        controlCollectionView.reloadData()
    }


}

extension CameraControlView: UICollectionViewDelegate, UICollectionViewDataSource {
    //cell 数量
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cameraControlDatas.count
    }
    //cell 具体内容
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CameraControlCell.reusableIdentifier, for: indexPath) as! CameraControlCell
        cell.titleLabel.text = cameraControlDatas[indexPath.item].title
        if cameraControlDatas[indexPath.item].isSelected {
            cell.iconView.image = cameraControlDatas[indexPath.item].selectedIcon
            if indexPath.item == 3 {
                cell.titleLabel.textColor = .custom(.blue_2da3f6)
            }

        }else{
            cell.iconView.image = cameraControlDatas[indexPath.item].normalIcon
            cell.titleLabel.textColor = .custom(.black_3f4663)
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        cameraControlDatas[indexPath.item].isSelected = !cameraControlDatas[indexPath.item].isSelected
        //刷新
        collectionView.reloadData()
        clickCallback?(indexPath.item)
    }
    
}

#endif
