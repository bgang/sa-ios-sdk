//
//  ClarityCell.swift
//  ZhiTing
//
//  Created by macbook on 2022/5/19.
//

import UIKit

class ClarityCell: UICollectionViewCell, ReusableView {
    
    //图标
    lazy var clarityLabel = Label().then {
        $0.textColor = .custom(.white_ffffff)
        $0.layer.cornerRadius = 15
        $0.layer.borderColor = UIColor.custom(.white_ffffff).cgColor
        $0.layer.borderWidth = 1
        $0.clipsToBounds = true
        $0.textAlignment = .center
        $0.font = .font(size: 12 , type: .medium)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setConstrains()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        backgroundColor = .clear
        contentView.addSubview(clarityLabel)
    }
    
    func setConstrains(){
        clarityLabel.snp.makeConstraints {
            $0.top.left.right.bottom.equalToSuperview()
        }
                
    }

}
