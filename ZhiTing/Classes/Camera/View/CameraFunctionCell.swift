//
//  CameraFunctionCell.swift
//  ZhiTing
//
//  Created by macbook on 2022/5/5.
//

import UIKit

class CameraFunctionCell: UICollectionViewCell, ReusableView {
    
    //图标
    lazy var iconView = ImageView().then {
        $0.contentMode = .scaleAspectFit
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setConstrains()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        iconView.image = nil
    }
    
    func setupViews() {
        contentView.addSubview(iconView)
    }
    
    func setConstrains(){
        iconView.snp.makeConstraints {
            $0.top.left.right.bottom.equalToSuperview()
        }
                
    }

}
