//
//  CircleConsoleView.swift
//  ZhiTing
//
//  Created by macbook on 2022/4/27.
//

import UIKit

enum CircleConsoleMoveDirection {
    case CircleConsoleMoveDirectionUp
    case CircleConsoleMoveDirectionDown
    case CircleConsoleMoveDirectionLeft
    case CircleConsoleMoveDirectionRight
    case CircleConsoleMoveDirectionNone
    case CircleConsoleMoveDirectionStop
    case CircleConsoleMoveDirectionLeftStop
    case CircleConsoleMoveDirectionRightStop
    case CircleConsoleMoveDirectionUpStop
    case CircleConsoleMoveDirectionDownStop
}

class CircleConsoleView: UIView {

    var moveCallback: ((_ direction: CircleConsoleMoveDirection) -> ())?
    var stopCallback: ((_ direction: CircleConsoleMoveDirection) -> ())?
    
    let centerViewX = 0.0
    let centerViewY = 0.0
    let deltaRadius = 0.0
    var tempPoint = CGPoint()
    //移动方向
    var direction = CircleConsoleMoveDirection.CircleConsoleMoveDirectionNone
    var isPortrait = true
    
    lazy var bgImageView = UIImageView().then {
        
        $0.image = .assets(.protrait_ConsoleBackgroundImage_normal)
    }

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
        setupConstraints()
        layoutIfNeeded()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    private func setupViews(){
        addSubview(bgImageView)
    }
    
    private func setupConstraints(){
        bgImageView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    
    
    private func ifYuntaiViewIsIntheBigView(point:CGPoint) -> Bool{
        let delX = fabs(point.x - centerViewX)
        let delY = fabs(point.y - centerViewY)
        
        if (delX * delX + delY * delY ) < deltaRadius {
            return true
        }
        return false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let point = (touch as AnyObject).location(in: self)
            let tranPoint = CGPoint(x: point.x - self.bounds.width / 2, y: point.y - self.bounds.height / 2)
            direction = self.determineCameraDirection(translation: tranPoint)
            switch direction {
            case .CircleConsoleMoveDirectionNone:
                bgImageView.image = isPortrait ? .assets(.protrait_ConsoleBackgroundImage_normal) : .assets(.landscape_ConsoleBackgroundImage_normal)
            case .CircleConsoleMoveDirectionUp:
                bgImageView.image = isPortrait ? .assets(.protrait_ConsoleBackgroundImage_active_up) : .assets(.landscape_ConsoleBackgroundImage_active_up)
            case .CircleConsoleMoveDirectionLeft:
                bgImageView.image = isPortrait ? .assets(.protrait_ConsoleBackgroundImage_active_left) : .assets(.landscape_ConsoleBackgroundImage_active_left)
            case .CircleConsoleMoveDirectionDown:
                bgImageView.image = isPortrait ? .assets(.protrait_ConsoleBackgroundImage_active_down) : .assets(.landscape_ConsoleBackgroundImage_active_down)
            case .CircleConsoleMoveDirectionRight:
                bgImageView.image = isPortrait ? .assets(.protrait_ConsoleBackgroundImage_active_right) : .assets(.landscape_ConsoleBackgroundImage_active_right)
            default:
                break
            }
            self.moveCallback?(direction)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        //结束上一次移动的操作
        self.stopCallback?(direction)
        //圆盘设置初始图片
        bgImageView.image = isPortrait ? .assets(.protrait_ConsoleBackgroundImage_normal) : .assets(.landscape_ConsoleBackgroundImage_normal)
        direction = .CircleConsoleMoveDirectionNone
    }
    
    private func determineCameraDirection(translation: CGPoint ) -> CircleConsoleMoveDirection{
        if translation.y == 0 || abs(translation.x / translation.y) >= 1.0 {
            if translation.x > 0.0 {
                //向右
                direction = .CircleConsoleMoveDirectionRight
            }else{
                //想左
                direction = .CircleConsoleMoveDirectionLeft
            }
        }else if translation.x == 0.0 || abs(translation.y / translation.x) >= 1.0 {
            if translation.y > 0.0 {
                direction = .CircleConsoleMoveDirectionDown
            }else{
                direction = .CircleConsoleMoveDirectionUp
            }
        }else{
            direction = .CircleConsoleMoveDirectionNone
        }
        
        return direction
    }
    
}
