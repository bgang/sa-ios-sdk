//
//  ChangeClarityView.swift
//  ZhiTing
//
//  Created by macbook on 2022/5/17.
//

import UIKit

struct Clarity {
    let smooth = 0
    let standard = 1
    let hd = 2
    let clear2K = 3
}

class ChangeClarityView: UIView {
    
    var clickCallback: ((_ index: Int) -> ())?
    var tapCallback: (() -> ())?
    var selectedClarity = Clarity().smooth
    
    private lazy var cover = UIView().then {
        $0.backgroundColor = .black.withAlphaComponent(0.5)
        $0.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(removeAction)))
    }
    
    //控制列表
    private lazy var flowLayout = UICollectionViewFlowLayout().then {
        $0.itemSize = CGSize(width: 80, height:  30)
        //行列间距
        $0.minimumLineSpacing = 20
        $0.minimumInteritemSpacing = 30
        $0.sectionInset = .init(top: 27, left: 30, bottom: 0, right: 30)
    }
    
    lazy var collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout).then{
        $0.backgroundColor = .clear
        $0.delegate = self
        $0.dataSource = self
        $0.showsHorizontalScrollIndicator = false
        $0.showsVerticalScrollIndicator = false
        $0.alwaysBounceVertical = true
        $0.alwaysBounceHorizontal = false
        $0.register(ClarityCell.self, forCellWithReuseIdentifier: ClarityCell.reusableIdentifier)
        $0.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(tapAction(tap:))))
    }

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
        layoutIfNeeded()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    private func setupViews(){
        addSubview(cover)
        cover.addSubview(collectionView)
    }
    
    private func setupConstraints(){
        cover.snp.makeConstraints {
            $0.top.left.right.bottom.equalToSuperview()
        }
        
        collectionView.snp.makeConstraints {
            $0.top.left.right.bottom.equalToSuperview()
        }

    }
    
    public func updateUI(isPortrait: Bool){
        if isPortrait {
            collectionView.snp.remakeConstraints {
                $0.top.left.right.bottom.equalToSuperview()
            }
        }else{
            collectionView.snp.remakeConstraints {
                $0.centerY.equalToSuperview()
                $0.left.right.equalToSuperview()
                $0.height.equalTo(270)
            }
        }
    }
    
    @objc
    private func tapAction(tap: UITapGestureRecognizer){
        let point = tap.location(in: collectionView)
        let index = collectionView.indexPathForItem(at: point)
        
        if index != nil {
            self.collectionView(collectionView, didSelectItemAt: index!)
        }else{
            self.tapCallback?()
        }
    }

    @objc
    private func removeAction(){
        self.tapCallback?()
    }
}

extension ChangeClarityView: UICollectionViewDelegate, UICollectionViewDataSource {
    //cell 数量
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    //cell 具体内容
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ClarityCell.reusableIdentifier, for: indexPath) as! ClarityCell
        switch indexPath.item {
        case 0:
            cell.clarityLabel.text = "流畅 180P"
        case 1:
            cell.clarityLabel.text = "标清 360P"
        case 2:
            cell.clarityLabel.text = "高清 720P"
        case 3:
            cell.clarityLabel.text = "超清 2K"
        default:
            break
        }
        
        if indexPath.item == selectedClarity{
            cell.clarityLabel.textColor = .custom(.oringe_f6ae1e)
            cell.clarityLabel.layer.borderColor = UIColor.custom(.oringe_f6ae1e).cgColor
        }else{
            cell.clarityLabel.textColor = .custom(.white_ffffff)
            cell.clarityLabel.layer.borderColor = UIColor.custom(.white_ffffff).cgColor
        }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        clickCallback?(indexPath.item)
        
    }
    
}
