//
//  CameraContrlCell.swift
//  ZhiTing
//
//  Created by macbook on 2022/5/5.
//

import UIKit

class CameraControlCell: UICollectionViewCell, ReusableView {
    
    //图标
    lazy var iconView = ImageView().then {
        $0.contentMode = .scaleAspectFit
    }
    
    lazy var titleLabel = Label().then {
        $0.font = .font(size: 10, type: .medium)
        $0.textAlignment = .center
        $0.textColor = .custom(.black_3f4663)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setConstrains()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        iconView.image = nil
    }
    
    func setupViews() {
        contentView.addSubview(iconView)
        contentView.addSubview(titleLabel)
    }
    
    func setConstrains(){
        iconView.snp.makeConstraints {
            $0.top.equalTo(25)
            $0.width.height.equalTo(25)
            $0.centerX.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(iconView.snp.bottom).offset(10)
            $0.centerX.equalToSuperview()
            $0.left.right.equalToSuperview()
        }
                
    }

}
