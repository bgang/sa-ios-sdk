//
//  RemotePlaybackTimeCell.swift
//  ZhiTing
//
//  Created by macbook on 2022/5/11.
//

import UIKit

class RemotePlaybackTimeCell: UICollectionViewCell,ReusableView {
    
    //是否选中，更改UI
    var isChoose: Bool? {
        didSet {
            guard let result = isChoose else {return}
            if result {//已选择当前cell,展开
                timeLabel.textColor = .custom(.blue_3699ff)
                timeLabel.layer.borderColor = UIColor.custom(.blue_3699ff).cgColor
            }else{
                timeLabel.textColor = .custom(.black_3f4663)
                timeLabel.layer.borderColor = UIColor.custom(.gray_94a5be).cgColor
            }
        }
    }

    //Label
    lazy var timeLabel = Label().then {
        $0.font = .font(size: 12, type: .medium)
        $0.backgroundColor = .custom(.white_ffffff)
        $0.textColor = .custom(.black_3f4663)
        $0.textAlignment = .center
        $0.layer.borderColor = UIColor.custom(.gray_94a5be).cgColor
        $0.layer.borderWidth = 0.5
        $0.layer.cornerRadius = 2
        $0.clipsToBounds = true
        $0.isUserInteractionEnabled = true
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstrains()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupViews(){
        contentView.addSubview(timeLabel)
    }

    private func setupConstrains(){
        timeLabel.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
