//
//  CameraHeaderView.swift
//  ZhiTing
//
//  Created by macbook on 2022/5/17.
//

import UIKit

class CameraHeaderView: UIView {
    
    var gobackCallback: (() -> ())?

    lazy var backArrow = ImageView().then {
        $0.image = .assets(.nav_back_white)
        $0.contentMode = .scaleAspectFit
        $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goback)))
        $0.isUserInteractionEnabled = true
    }

    lazy var nameLabel = Label().then {
        $0.text = ""
        $0.font = .font(size: 18, type: .bold)
        $0.textAlignment = .left
        $0.textColor = .custom(.white_ffffff)
        $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goback)))
        $0.isUserInteractionEnabled = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func goback() {
        gobackCallback?()
    }
    
    private func setupViews() {
        backgroundColor = .clear
        addSubview(backArrow)
        addSubview(nameLabel)
    }
    
    private func setupConstraints() {
        backArrow.snp.makeConstraints {
            $0.centerY.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(15).priority(.high)
            $0.width.equalTo(8)
            $0.height.equalTo(15)
        }
        
        nameLabel.snp.makeConstraints {
            $0.centerY.equalTo(backArrow.snp.centerY)
            $0.left.equalTo(backArrow.snp.right).offset(15)
            $0.width.greaterThanOrEqualTo(100)
            $0.height.equalTo(20)
        }
    }
}
