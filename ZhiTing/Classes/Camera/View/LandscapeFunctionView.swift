//
//  LandscapeFunctionView.swift
//  ZhiTing
//
//  Created by macbook on 2022/5/17.
//
#if !(targetEnvironment(simulator))
import UIKit

class LandscapeFunctionView: UIView {
    
    var clickCallback: ((_ index: Int) -> ())?
        
    //控制列表
    private lazy var flowLayout = UICollectionViewFlowLayout().then {
        $0.itemSize = CGSize(width: 50, height:  50)
        //行列间距
        $0.minimumLineSpacing = 15
        $0.minimumInteritemSpacing = 10
        $0.sectionInset = .init(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    lazy var collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout).then{
        $0.backgroundColor = .clear
        $0.delegate = self
        $0.dataSource = self
        $0.showsHorizontalScrollIndicator = false
        $0.showsVerticalScrollIndicator = false
        $0.alwaysBounceVertical = true
        $0.alwaysBounceHorizontal = false
        $0.register(CameraFunctionCell.self, forCellWithReuseIdentifier: CameraFunctionCell.reusableIdentifier)
    }

    var cameraControlDatas = [CameraFuctionModel]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
        setupControlDatas()
        layoutIfNeeded()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    private func setupViews(){
        addSubview(collectionView)
    }
    
    private func setupConstraints(){

        collectionView.snp.makeConstraints {
            $0.top.left.right.bottom.equalToSuperview()
        }

    }
    
    private func setupControlDatas(){
        let cameraShot = CameraFuctionModel()
        cameraShot.normalIcon = .assets(.camera_shot_landscape)
        cameraShot.selectedIcon = .assets(.camera_shot_landscape)
        let cameraRecord = CameraFuctionModel()
        cameraRecord.normalIcon = .assets(.camera_record_landscape)
        cameraRecord.selectedIcon = .assets(.camera_record_selected_landscape)
        let cameraTalk = CameraFuctionModel()
        cameraTalk.normalIcon = .assets(.camera_talk_landscape)
        cameraTalk.selectedIcon = .assets(.camera_talk_selected_landscape)
        let cameraAudio = CameraFuctionModel()
        cameraAudio.normalIcon = .assets(.camera_audio_landscape)
        cameraAudio.selectedIcon = .assets(.camera_audio_selected_landscape)

        cameraControlDatas = [cameraShot, cameraRecord, cameraTalk, cameraAudio]
        //刷新数据
        collectionView.reloadData()
    }


}

extension LandscapeFunctionView: UICollectionViewDelegate, UICollectionViewDataSource {
    //cell 数量
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cameraControlDatas.count
    }
    //cell 具体内容
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CameraFunctionCell.reusableIdentifier, for: indexPath) as! CameraFunctionCell
        if cameraControlDatas[indexPath.item].isSelected {
            cell.iconView.image = cameraControlDatas[indexPath.item].selectedIcon
        }else{
            cell.iconView.image = cameraControlDatas[indexPath.item].normalIcon
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        cameraControlDatas[indexPath.item].isSelected = !cameraControlDatas[indexPath.item].isSelected
        //刷新
        collectionView.reloadData()
        clickCallback?(indexPath.item)
    }
    
}
#endif
