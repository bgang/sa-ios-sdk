//
//  CameraDownControlView.swift
//  ZhiTing
//
//  Created by macbook on 2022/5/7.
//

import UIKit

class CameraDownControlView: UIView {
    
    var clickCallback: ((_ index: Int) -> ())?

    lazy var bgImgView = ImageView().then {
        $0.image = .assets(.camera_shadow_down)
        $0.isUserInteractionEnabled = true
    }
    
    lazy var fullScreenBtn = Button().then {
        $0.setImage(.assets(.camera_intoLanscape), for: .normal)
        $0.isEnhanceClick = true
        $0.clickCallBack = { [weak self] _ in
            self?.clickCallback?(0)
        }
    }
    
    lazy var leftRightBtn = Button().then {
        $0.setImage(.assets(.camera_leftRight_white), for: .normal)
        $0.setImage(.assets(.camera_leftRight_white), for: .selected)
        $0.isEnhanceClick = true
        $0.clickCallBack = { [weak self] _ in
            self?.clickCallback?(1)
        }
    }

    lazy var upDownBtn = Button().then {
        $0.setImage(.assets(.camera_upDown_white), for: .normal)
        $0.setImage(.assets(.camera_upDown_white), for: .selected)
        $0.isEnhanceClick = true
        $0.clickCallBack = { [weak self] _ in
            self?.clickCallback?(2)
        }
    }

    lazy var clarityBtn = Button().then {
        $0.setTitle("流畅", for: .normal)
        $0.titleLabel?.font = .font(size: 14, type: .medium)
        $0.setTitleColor(.custom(.white_ffffff), for: .normal)
        $0.layer.borderColor = UIColor.custom(.white_ffffff).cgColor
        $0.layer.borderWidth = 1
        $0.layer.cornerRadius = 10
        $0.clipsToBounds = true
        $0.isEnhanceClick = true
        $0.clickCallBack = { [weak self] _ in
            self?.clickCallback?(3)
        }
    }

    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
        layoutIfNeeded()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    private func setupViews(){
        addSubview(bgImgView)
        bgImgView.addSubview(fullScreenBtn)
        bgImgView.addSubview(leftRightBtn)
        bgImgView.addSubview(upDownBtn)
        bgImgView.addSubview(clarityBtn)
    }
    
    private func setupConstraints(){
        bgImgView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        fullScreenBtn.snp.makeConstraints {
            $0.right.equalTo(-15)
            $0.bottom.equalTo(-10)
            $0.width.height.equalTo(20)
        }
        leftRightBtn.snp.makeConstraints {
            $0.right.equalTo(fullScreenBtn.snp.left).offset(-20)
            $0.bottom.equalTo(-10)
            $0.width.height.equalTo(20)
        }
        upDownBtn.snp.makeConstraints {
            $0.right.equalTo(leftRightBtn.snp.left).offset(-20)
            $0.bottom.equalTo(-10)
            $0.width.height.equalTo(20)
        }
        clarityBtn.snp.makeConstraints {
            $0.right.equalTo(upDownBtn.snp.left).offset(-20)
            $0.bottom.equalTo(-10)
            $0.width.equalTo(50)
            $0.height.equalTo(20)
        }
    }
    
    public func updateUI(isPortrait: Bool){
        if isPortrait {//竖屏
            leftRightBtn.isHidden = true
            upDownBtn.isHidden = true
            clarityBtn.snp.remakeConstraints {
                $0.right.equalTo(fullScreenBtn.snp.left).offset(-20)
                $0.bottom.equalTo(-10)
                $0.width.equalTo(50)
                $0.height.equalTo(20)
            }
        }else{
            leftRightBtn.isHidden = false
            upDownBtn.isHidden = false
            clarityBtn.snp.remakeConstraints {
                $0.right.equalTo(upDownBtn.snp.left).offset(-20)
                $0.bottom.equalTo(-10)
                $0.width.equalTo(50)
                $0.height.equalTo(20)
            }
        }
    }
    
}
