//
//  PublicConfig.swift
//  ijkswift
//
//  Created by macbook on 2018/9/26.
//  Copyright © 2018 YTTV. All rights reserved.
//

import UIKit

/// 延时时间
let DELAY_TIME: TimeInterval = 5

let DOWNLOAD_SPEED_NOTIFICATION = "downloadspeednotification"
let UPLOAD_SPEED_NOTIFICATION = "uploadspeednotification"

func ZTLog<T>(_ message: T, file: NSString = #file, method: String = #function, line: Int = #line){
    #if DEBUG
        let date = Date()
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        let strNowTime = timeFormatter.string(from: date) as String
        print("\(strNowTime) \(method)[\(line)]: \(message)")
    #endif
}
