//
//  SADeviceViewController.swift
//  ZhiTing
//
//  Created by iMac on 2021/4/20.
//

import UIKit


class SADeviceViewController: BaseViewController {
    var device_id = 0
    
    var area = Area()
    
    var cellsArray = [UITableViewCell]()
    
    var maintenanceTipsAlert: TipsAlertView?
    
    /// sa维护状态 mode, 是否进入维护模式的用户
    var saMaintenanceState: (mode: Int, isMaintainer: Bool) = (0, false)
    
    private lazy var settingButton = Button().then {
        $0.setImage(UIImage(systemName: "gearshape")?.withTintColor(.white), for: .normal)
        $0.tintColor = .white
        $0.setTitleColor(.white, for: .normal)
        $0.frame.size = CGSize(width: 18, height: 18)
        $0.clickCallBack = { [weak self] _ in
            guard let self = self else { return }
            let vc = DeviceDetailViewController()
            vc.area = self.area
            vc.device_id = self.device_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    lazy var deviceImg = ImageView().then {
        $0.contentMode = .scaleAspectFit
        $0.image = .assets(.device_sa)
    }
    
    private lazy var coverView = ImageView().then {
        $0.image = .assets(.sa_background)
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
    }
    
    private lazy var maintainModeBtn = Button().then {
        $0.setImage(.assets(.btn_maintain_normal), for: .normal)
        $0.adjustsImageWhenHighlighted = false
        $0.isHidden = true
    }
    
    private lazy var maintainStatusLabel = Label().then {
        $0.text = "已进入维护模式"
        $0.textAlignment = .center
        $0.textColor = .custom(.blue_2da3f6)
        $0.font = .font(size: 14, type: .bold)
        $0.isHidden = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationBarAppearance.configureWithTransparentBackground()
        navigationBarAppearance.shadowImage = UIImage()
        navigationBarAppearance.backgroundImage = nil
        navigationBarAppearance.backgroundColor = UIColor.clear
        navigationBarAppearance.titleTextAttributes = [NSAttributedString.Key.font: UIFont.font(size: 18, type: .bold), NSAttributedString.Key.foregroundColor: UIColor.custom(.white_ffffff)]
        
        navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
        navigationController?.navigationBar.standardAppearance = navigationBarAppearance
        navigationItem.title = "设备详情".localizedString
        navBackBtn.setImage(.assets(.nav_back_white), for: .normal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: settingButton)
        
        requestNetwork()
    }

    private lazy var softwareCell = ValueDetailCell().then {
        $0.title.text = "软件升级".localizedString
        $0.line.isHidden = true
        $0.bottomLine.isHidden = false
        $0.valueLabel.text = " "
    }
    
    private lazy var firmwareCell = ValueDetailCell().then {
        $0.title.text = "固件升级".localizedString
        $0.line.isHidden = true
        $0.bottomLine.isHidden = false
        $0.valueLabel.text = " "
    }

    private lazy var proEditionBtn = ImageTitleButton(frame: .zero, icon: nil, title: "进入专业版".localizedString, titleColor: UIColor.custom(.white_ffffff), backgroundColor: UIColor.custom(.blue_2da3f6))

    private lazy var tableView = UITableView(frame: .zero, style: .plain).then {
        $0.separatorStyle = .none
        $0.backgroundColor = .custom(.gray_f6f8fd)
        $0.rowHeight = UITableView.automaticDimension
        $0.estimatedRowHeight = ZTScaleValue(50)
        if #available(iOS 15.0, *) {
            $0.sectionHeaderTopPadding = 0
        }
        $0.delegate = self
        $0.dataSource = self
    }
    
    private lazy var deleteButton = ImageTitleButton(frame: .zero, icon: nil, title: "删除设备".localizedString, titleColor: UIColor.custom(.blue_2da3f6), backgroundColor: UIColor.custom(.white_ffffff)).then {
        $0.isHidden = true
        $0.layer.borderColor = UIColor.custom(.blue_2da3f6).cgColor
        $0.layer.borderWidth = 0.5
    }
    
    var tipsAlert: DeleteSAAlert?
    
    /// sa是否绑定云端
    var isBindCloud = false
    
    override func setupViews() {
        view.backgroundColor = .custom(.gray_f6f8fd)
        view.addSubview(coverView)
        coverView.addSubview(deviceImg)
        coverView.addSubview(maintainStatusLabel)
        view.addSubview(maintainModeBtn)
        view.addSubview(tableView)
        view.addSubview(deleteButton)
        view.addSubview(proEditionBtn)
        
        deleteButton.clickCallBack = { [weak self] in
            guard let self = self else { return }
            self.tipsAlert = DeleteSAAlert.show(area: self.area, isBindCloud: self.isBindCloud) { [weak self] is_migration_sa, is_del_cloud_disk in
                guard let self = self else { return }
                Task { [weak self] in
                    guard let self = self else { return }
                    await self.deleteSA(is_migration_sa: is_migration_sa, is_del_cloud_disk: is_del_cloud_disk)
                }

            } loginClick: { [weak self] in
                let vc = LoginViewController()
                vc.loginComplete = { [weak self] in
                    self?.tipsAlert?.removeFromSuperview()
                    self?.navigationController?.popToRootViewController(animated: true)
                }
                vc.hidesBottomBarWhenPushed = true
                let nav = BaseNavigationViewController(rootViewController: vc)
                nav.modalPresentationStyle = .overFullScreen
                AppDelegate.shared.appDependency.tabbarController.present(nav, animated: true, completion: nil)
            }
        }
        
        proEditionBtn.clickCallBack = { [weak self] in
            guard let self = self else { return }
            self.enterPro()
        }
        
        maintainModeBtn.clickCallBack = { [weak self] _ in
            guard let self = self else { return }
            self.enterMaintenanceAlert()
        }
        
        maintainModeBtn.touchDownCallback = { btn in
            btn.setImage(.assets(.btn_maintain_select), for: .normal)
        }

        maintainModeBtn.touchOutsideCallback = { btn in
            btn.setImage(.assets(.btn_maintain_normal), for: .normal)
        }

    }
    
    override func setupConstraints() {
        
        coverView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(-Screen.k_nav_height)
            $0.left.right.equalToSuperview()
            $0.height.equalTo(ZTScaleValue(300) + Screen.k_nav_height)
        }
        
        deviceImg.snp.makeConstraints {
            $0.width.height.equalTo(ZTScaleValue(120))
            $0.bottom.equalTo(maintainModeBtn.snp.top).offset(-25)
            $0.centerX.equalToSuperview()
        }
        
        maintainModeBtn.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.bottom.equalTo(coverView.snp.bottom).offset(-20)
            $0.width.equalTo(125)
            $0.height.equalTo(35)
        }
        
        maintainStatusLabel.snp.makeConstraints {
            $0.edges.equalTo(maintainModeBtn)
        }

        tableView.snp.makeConstraints {
            $0.top.equalTo(coverView.snp.bottom).offset(ZTScaleValue(10))
            $0.left.right.equalToSuperview()
            $0.bottom.equalTo(deleteButton.snp.top).offset(-15)
        }
        
        deleteButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ZTScaleValue(15))
            $0.right.equalToSuperview().offset(-ZTScaleValue(15))
            $0.height.equalTo(50)
            $0.bottom.equalToSuperview().offset(-ZTScaleValue(10) - Screen.bottomSafeAreaHeight)
        }
        
        proEditionBtn.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ZTScaleValue(15))
            $0.right.equalToSuperview().offset(-ZTScaleValue(15))
            $0.height.equalTo(50)
            $0.bottom.equalToSuperview().offset(-ZTScaleValue(10) - Screen.bottomSafeAreaHeight)
        }
    }
    
    private func enterMaintenanceAlert() {
        if saMaintenanceState.1 == true {
            let vc = SAMaintainViewController()
            vc.resetCell.switchBtn.setIsOn(saMaintenanceState.0 == 3)
            navigationController?.pushViewController(vc, animated: true)
        } else {
            maintenanceTipsAlert = TipsAlertView.show(message: "提示\n仅可一名成员进入维护模式页面，进入后其他成员无法进入，是否进入？", sureCallback: { [weak self] in
                guard let self = self else { return }
                self.maintenanceTipsAlert?.isSureBtnLoading = true
                ApiServiceManager.shared.getMaintenanceToken { [weak self] resp in
                    guard let self = self else { return }
                    self.maintenanceTipsAlert?.isSureBtnLoading = false
                    self.maintenanceTipsAlert?.removeFromSuperview()
                    let vc = SAMaintainViewController()
                    if resp.access_token != "" {
                        AuthManager.shared.currentArea.sa_user_token = resp.access_token
                        AuthManager.shared.currentArea.isAllowedGetToken = true
                        AreaCache.cacheArea(areaCache: AuthManager.shared.currentArea.toAreaCache())
                    }

                    self.navigationController?.pushViewController(vc, animated: true)

                } failureCallback: { [weak self] code, err in
                    guard let self = self else { return }
                    self.maintenanceTipsAlert?.isSureBtnLoading = false
                    self.showToast(string: err)
                }

                
            }, removeWithSure: false)
        }

        
        

    }
    
    

    private func requestNetwork() {
        if area.is_bind_sa {
            ApiServiceManager.shared.getSAExtensions(area: area) { [weak self] response in
                self?.area.extensions = response.extension_names
            } failureCallback: { [weak self] code, err in
                self?.showToast(string: err)
            }
            
            /// 获取sa状态 判断是否维护状态
            ApiServiceManager.shared.getSAStatus(area: area) { [weak self] response in
                guard let self = self else { return }
                switch response.mode {
                case 0: /// 正常
                    let navigationBarAppearance = UINavigationBarAppearance()
                    navigationBarAppearance.configureWithTransparentBackground()
                    navigationBarAppearance.shadowImage = UIImage()
                    navigationBarAppearance.backgroundImage = nil
                    navigationBarAppearance.backgroundColor = UIColor.clear
                    navigationBarAppearance.titleTextAttributes = [NSAttributedString.Key.font: UIFont.font(size: 18, type: .bold), NSAttributedString.Key.foregroundColor: UIColor.custom(.black_333333)]
                    self.navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
                    self.navigationController?.navigationBar.standardAppearance = navigationBarAppearance
                    self.navBackBtn.setImage(.assets(.navigation_back), for: .normal)
                    self.coverView.image = nil
                    self.maintainModeBtn.isHidden = true
                    self.maintainStatusLabel.isHidden = true
                    self.saMaintenanceState = (0, false)
                    
                case 1: /// 维护模式等待连接
                    let navigationBarAppearance = UINavigationBarAppearance()
                    navigationBarAppearance.configureWithTransparentBackground()
                    navigationBarAppearance.shadowImage = UIImage()
                    navigationBarAppearance.backgroundImage = nil
                    navigationBarAppearance.backgroundColor = UIColor.clear
                    navigationBarAppearance.titleTextAttributes = [NSAttributedString.Key.font: UIFont.font(size: 18, type: .bold), NSAttributedString.Key.foregroundColor: UIColor.custom(.white_ffffff)]
                    
                    self.navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
                    self.navigationController?.navigationBar.standardAppearance = navigationBarAppearance
                    self.navBackBtn.setImage(.assets(.nav_back_white), for: .normal)
                    self.coverView.image = .assets(.sa_background)
                    self.maintainModeBtn.isHidden = false
                    self.maintainStatusLabel.isHidden = true
                    self.saMaintenanceState = (1, false)
                    
                case 2, 3: /// 维护模式已连接
                    let navigationBarAppearance = UINavigationBarAppearance()
                    navigationBarAppearance.configureWithTransparentBackground()
                    navigationBarAppearance.shadowImage = UIImage()
                    navigationBarAppearance.backgroundImage = nil
                    navigationBarAppearance.backgroundColor = UIColor.clear
                    navigationBarAppearance.titleTextAttributes = [NSAttributedString.Key.font: UIFont.font(size: 18, type: .bold), NSAttributedString.Key.foregroundColor: UIColor.custom(.white_ffffff)]
                    
                    self.navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
                    self.navigationController?.navigationBar.standardAppearance = navigationBarAppearance
                    self.navBackBtn.setImage(.assets(.nav_back_white), for: .normal)
                    self.coverView.image = .assets(.sa_background)
                    
                    ApiServiceManager.shared.getMaintenanceToken { [weak self] _ in
                        guard let self = self else { return }
                        self.maintainModeBtn.isHidden = false
                        self.maintainStatusLabel.isHidden = true
                        self.saMaintenanceState = (response.mode, true)
                    } failureCallback: { [weak self] code, err in
                        guard let self = self else { return }
                        self.maintainModeBtn.isHidden = true
                        self.maintainStatusLabel.isHidden = false
                        self.saMaintenanceState = (response.mode, false)
                    }


                    
                    
                    
                default:
                    break
                }
            }

        }
        
        ApiServiceManager.shared.deviceDetail(area: area, device_id: device_id) { [weak self] (response) in
            guard let self = self else { return }
            self.navigationItem.title = response.device_info.name
            

            if (response.device_info.permissions.delete_device || response.device_info.permissions.update_device) {
                self.settingButton.isHidden = false
            } else {
                self.settingButton.isHidden = true
            }
            
            self.getRolePermission()
            self.getUserDetail()
            
        } failureCallback: { [weak self] (code, err) in
            self?.showToast(string: err)
        }
        
        
    }
    
    private func getRolePermission() {
        ApiServiceManager.shared.rolesPermissions(area: area, user_id: area.sa_user_id) { [weak self] response in
            guard let self = self else { return }
            self.cellsArray.removeAll()

            if response.permissions.sa_software_upgrade {
                self.cellsArray.append(self.softwareCell)
            }
            
            if response.permissions.sa_firmware_upgrade {
                self.cellsArray.append(self.firmwareCell)
            }
            
            self.tableView.reloadData()
        } failureCallback: { [weak self] (code, err) in
            guard let self = self else { return }
            self.showToast(string: err)
        }

    }
    
    private func getUserDetail() {
        ApiServiceManager.shared.userDetail(area: area, id: area.sa_user_id) { [weak self] response in
            guard let self = self else { return }
            self.isBindCloud = response.area?.is_bind_cloud == true
            self.deleteButton.isHidden = !response.is_owner
            
            let width = (Screen.screenWidth - ZTScaleValue(45)) / 2
            if response.is_owner {
                self.deleteButton.snp.remakeConstraints {
                    $0.left.equalToSuperview().offset(ZTScaleValue(15))
                    $0.height.equalTo(50)
                    $0.width.equalTo(width)
                    $0.bottom.equalToSuperview().offset(-ZTScaleValue(10) - Screen.bottomSafeAreaHeight)
                }
                
                self.proEditionBtn.snp.remakeConstraints {
                    $0.right.equalToSuperview().offset(-ZTScaleValue(15))
                    $0.height.equalTo(50)
                    $0.width.equalTo(width)
                    $0.bottom.equalToSuperview().offset(-ZTScaleValue(10) - Screen.bottomSafeAreaHeight)
                }
            } else {
                self.deleteButton.snp.remakeConstraints {
                    $0.left.equalToSuperview().offset(ZTScaleValue(15))
                    $0.right.equalToSuperview().offset(-ZTScaleValue(15))
                    $0.height.equalTo(50)
                    $0.bottom.equalToSuperview().offset(-ZTScaleValue(10) - Screen.bottomSafeAreaHeight)
                }
            }
            
            
        } failureCallback: { [weak self] code, err in
            guard let self = self else { return }
            self.showToast(string: err)
        }

    }
    
    @MainActor
    func retreiveSAToken() async throws {
        do {
            let _ = try await AsyncApiService.areaDetail(area: AuthManager.shared.currentArea)
        } catch {
            let response = try await AsyncApiService.getSAToken(area: authManager.currentArea)
            authManager.currentArea.isAllowedGetToken = true
            //更新数据库token
            authManager.currentArea.sa_user_token = response.sa_token
            AreaCache.cacheArea(areaCache: authManager.currentArea.toAreaCache())
        }
    }
    
    
    private func enterPro() {
        if UserManager.shared.isLogin {
            Task {
                do {
                    showLoadingView()
                    try await retreiveSAToken()
                    hideLoadingView()
                    let vc = ProEditionViewController(linkEnum: .proEdition)
                    let nav = BaseProNavigationViewController(rootViewController: vc)
                    nav.modalPresentationStyle = .fullScreen
                    present(nav, animated: true, completion: nil)
                } catch {
                    hideLoadingView()
                    TipsAlertView.show(
                        message: "当前终端无凭证或已过期，可通过云端找回凭证。".localizedString,
                        sureTitle: "如何找回".localizedString,
                        sureCallback: { [weak self] in
                            guard let self = self else { return }
                            let vc = GuideTokenViewController()
                            self.navigationController?.pushViewController(vc, animated: true)
                        },
                        cancelCallback: nil,
                        removeWithSure: true
                    )
                }
            }
        } else if authManager.isSAEnviroment {
            Task {
                do {
                    showLoadingView()
                    let _ = try await AsyncApiService.areaDetail(area: AuthManager.shared.currentArea)
                    hideLoadingView()
                    let vc = ProEditionViewController(linkEnum: .proEdition)
                    let nav = BaseProNavigationViewController(rootViewController: vc)
                    nav.modalPresentationStyle = .fullScreen
                    present(nav, animated: true, completion: nil)
                    
                } catch {
                    hideLoadingView()
                    if let err = error as? AsyncApiError {
                        if err.code == 2011 || err.code == 2010 || err.code == 5012 {
                            TipsAlertView.show(
                                message: "当前终端无凭证或已过期，可通过云端找回凭证。".localizedString,
                                sureTitle: "如何找回".localizedString,
                                sureCallback: { [weak self] in
                                    guard let self = self else { return }
                                    let vc = GuideTokenViewController()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                },
                                cancelCallback: nil,
                                removeWithSure: true
                            )
                        } else {
                            showToast(string: err.err)
                        }
                        
                    }
                }
            }
            
        }  else {
            showToast(string: "请在局域网内或登录后使用".localizedString)
        }
    }
    
    @MainActor
    private func deleteSA(is_migration_sa: Bool, is_del_cloud_disk: Bool) async {
        do {
            showLoadingView()
            /// 删除SA响应
            let response: DeleAreaResponse
            
            /// 同时云端家庭
            if is_migration_sa {
                let cloudAreaResponse = try await AsyncApiService.createArea(name: area.name, location_names: [], department_names: [], area_type: area.areaType)
                
                response = try await AsyncApiService.deleteSA(area: area, is_migration_sa: true, is_del_cloud_disk: is_del_cloud_disk, cloud_area_id: cloudAreaResponse.id, cloud_access_token: cloudAreaResponse.cloud_sa_user_info?.token)
            } else {
                response = try await AsyncApiService.deleteSA(area: area, is_migration_sa: false, is_del_cloud_disk: is_del_cloud_disk, cloud_area_id: nil, cloud_access_token: nil)
            }
            
            if response.remove_status == 3 { /// 移除成功
                hideLoadingView()
                AreaCache.deleteArea(id: area.id, sa_token: area.sa_user_token)
                if AreaCache.areaList().count == 0 {
                    self.authManager.currentArea = AreaCache.createArea(name: "我的家".localizedString, locations_name: [], sa_token: "unbind\(UUID().uuidString)", mode: .family).transferToArea()
                    if UserManager.shared.isLogin {
                        self.authManager.syncLocalAreasToCloud(needUpdateCurrentArea: true) { [weak self] in
                            guard let self = self else { return }
                            self.navigationController?.popToRootViewController(animated: true)
                            self.showToast(string: "删除成功".localizedString)
                        }
                    } else {
                        self.navigationController?.popToRootViewController(animated: true)
                        self.showToast(string: "删除成功".localizedString)
                    }
                    
                } else if self.authManager.currentArea.id == area.id && self.authManager.currentArea.sa_user_token == area.sa_user_token {
                    if let area = AreaCache.areaList().first {
                        self.authManager.currentArea = area
                    }
                    self.showToast(string: "删除成功".localizedString)
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    self.showToast(string: "删除成功".localizedString)
                    self.navigationController?.popViewController(animated: true)
                }
                
            } else if response.remove_status == 2 { /// 移除失败
                hideLoadingView()
                TipsAlertView.show(message: "提示\n\n删除智慧中心失败,请稍后再试", sureCallback: nil)
            } else if response.remove_status == 1 { /// 移除中
                hideLoadingView()
                
                let areas = AreaCache.areaList().filter({ $0.id != self.area.id })
                
                if self.area.needRebindCloud || self.area.cloud_user_id == -1 { /// 未同步到云端的家庭直接移除缓存
                    AreaCache.deleteArea(id: area.id, sa_token: area.sa_user_token)
                }

                if areas.count == 0 {
                    /// 若除了正在删除中的家庭外没有家庭了,则帮其自动创建一个
                    self.authManager.currentArea = AreaCache.createArea(name: "我的家".localizedString, locations_name: [], sa_token: "unbind\(UUID().uuidString)", mode: .family).transferToArea()
                    if UserManager.shared.isLogin {
                        self.authManager.syncLocalAreasToCloud(needUpdateCurrentArea: true) { [weak self] in
                            guard let self = self else { return }
                            WarningAlert.show(message: "删除数据和云盘文件需要一定时间，已为你后台运行，可在首页切换家庭/公司查看删除情况。".localizedString, sureTitle: "确定".localizedString) { [weak self] in
                                guard let self = self else { return }
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                        }
                    } else {
                        WarningAlert.show(message: "删除数据和云盘文件需要一定时间，已为你后台运行，可在首页切换家庭/公司查看删除情况。".localizedString, sureTitle: "确定".localizedString) { [weak self] in
                            guard let self = self else { return }
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    }
                } else {
                    /// 删除的是当前家庭时 帮其切换至对应局域网的家庭，若无则切换至第一个
                    if AuthManager.shared.currentArea.id == self.area.id {
                        if let area = areas.first(where: { $0.bssid == NetworkStateManager.shared.getWifiBSSID() }) {
                            AuthManager.shared.currentArea = area
                        } else {
                            if let area = areas.first {
                                AuthManager.shared.currentArea = area
                            }
                        }
                    }
                    
                    WarningAlert.show(message: "删除数据和云盘文件需要一定时间，已为你后台运行，可在首页切换家庭/公司查看删除情况。".localizedString, sureTitle: "确定".localizedString) { [weak self] in
                        guard let self = self else { return }
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
            }
            
        } catch {
            hideLoadingView()
            TipsAlertView.show(message: "提示\n\n删除智慧中心失败,请稍后再试", sureCallback: nil)
        }
        

    }

}

extension SADeviceViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cellsArray[indexPath.row]

    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch cellsArray[indexPath.row] {
        case softwareCell:
            let vc = SAUpdateViewController()
            vc.updateType = .software
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        case firmwareCell:
            let vc = SAUpdateViewController()
            vc.updateType = .firmware
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
    
    
}


