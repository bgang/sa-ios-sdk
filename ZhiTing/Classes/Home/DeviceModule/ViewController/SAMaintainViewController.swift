//
//  SAMaintainViewController.swift
//  ZhiTing
//
//  Created by iMac on 2022/7/4.
//

import Foundation
import UIKit


class SAMaintainViewController: BaseViewController {
    
    private lazy var changeOwnerCell = ValueDetailCell().then {
        $0.title.text = "更换拥有者"
    }
    
    var tipsAlert: TipsAlertView?

    lazy var resetCell = ResetProEditionCell()

    private lazy var tableView = UITableView(frame: .zero, style: .plain).then {
        $0.delegate = self
        $0.dataSource = self
        $0.backgroundColor = .custom(.gray_f6f8fd)
        $0.separatorStyle = .none
    }
    
    private lazy var quitMaintainBtn = ImageTitleButton(frame: .zero, icon: nil, title: "退出维护模式".localizedString, titleColor: UIColor.custom(.white_ffffff), backgroundColor: UIColor.custom(.blue_2da3f6))

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationBarAppearance.configureWithTransparentBackground()
        navigationBarAppearance.shadowImage = UIImage()
        navigationBarAppearance.backgroundImage = nil
        navigationBarAppearance.backgroundColor = UIColor.init(hex: "#3F4663")
        navigationBarAppearance.titleTextAttributes = [NSAttributedString.Key.font: UIFont.font(size: 18, type: .bold), NSAttributedString.Key.foregroundColor: UIColor.custom(.white_ffffff)]
        
        navigationController?.navigationBar.scrollEdgeAppearance = navigationBarAppearance
        navigationController?.navigationBar.standardAppearance = navigationBarAppearance
        navigationItem.title = "维护模式".localizedString
        navBackBtn.setImage(.assets(.nav_back_white), for: .normal)
    }
    
    override func setupViews() {
        view.addSubview(tableView)
        view.addSubview(quitMaintainBtn)
        
        quitMaintainBtn.clickCallBack = { [weak self] in
            guard let self = self else { return }
            self.quitMaintenance()
        }
        
        resetCell.switchBtn.stateChangeCallback = { [weak self] state in
            guard let self = self else { return }
            self.resetProPwd(state: state)
        }
        
    }

    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        quitMaintainBtn.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-15 - Screen.bottomSafeAreaHeight)
            $0.left.equalToSuperview().offset(15)
            $0.right.equalToSuperview().offset(-15)
            $0.height.equalTo(50)
        }
    }
    
    private func resetProPwd(state: Bool) {
        ApiServiceManager.shared.resetProPwd(is_reset: state, successCallback: nil) { [weak self] code, err in
            guard let self = self else { return }
            self.resetCell.switchBtn.setIsOn(!state)
        }
    }
    
    private func quitMaintenance() {
        tipsAlert = TipsAlertView.show(message: "提示\n设备处于维护模式，无操作15分钟后将自动退出，是否关闭页面？", sureCallback: { [weak self] in
            guard let self = self else { return }
            self.tipsAlert?.isSureBtnLoading = true
            ApiServiceManager.shared.exitMaintenance { [weak self] _ in
                guard let self = self else { return }
                self.tipsAlert?.isSureBtnLoading = false
                self.tipsAlert?.removeFromSuperview()
                self.showToast(string: "设备已退出维护模式")
                self.navigationController?.popViewController(animated: true)
            } failureCallback: { [weak self] code, err in
                guard let self = self else { return }
                self.tipsAlert?.isSureBtnLoading = false
                self.showToast(string: err)
            }

        }, removeWithSure: false)
        
        
       

    }
    
}

extension SAMaintainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return changeOwnerCell
        } else {
            return resetCell
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            tipsAlert = TipsAlertView.show(message: "更换拥有者\n是否将“拥有者”角色转移给当前成员？", sureCallback: { [weak self] in
                guard let self = self else { return }
                self.tipsAlert?.isSureBtnLoading = true
                ApiServiceManager.shared.changeOwner { [weak self] _ in
                    guard let self = self else { return }
                    self.tipsAlert?.isSureBtnLoading = false
                    self.tipsAlert?.removeFromSuperview()
                    self.showToast(string: "更换成功".localizedString)
                } failureCallback: { [weak self] code, err in
                    guard let self = self else { return }
                    self.tipsAlert?.isSureBtnLoading = false
                    self.showToast(string: err)
                }
    
            }, removeWithSure: false)
            
            
           

        }
    }

}
