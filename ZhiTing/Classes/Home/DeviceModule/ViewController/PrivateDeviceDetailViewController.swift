//
//  PrivateDeviceViewController.swift
//  ZhiTing
//
//  Created by iMac on 2022/5/18.
//


import UIKit

class PrivateDeviceDetailViewController: BaseViewController {
    var area: Area?
    var renameCallback: ((String) -> ())?
    var device: Device? {
        didSet {
            guard let device = device else { return }
            header.icon.image = device.logo_image
            header.deviceTypeLabel.text = device.name
            nameCell.valueLabel.text = device.name

            cells = [nameCell]
            tableView.reloadData()
        }
    }
    
    private lazy var cells = [ValueDetailCell]()

    private lazy var header = DeviceDetailHeader(frame: CGRect(x: 0, y: 0, width: Screen.screenWidth, height: 185))
    
    
    private lazy var tableView = UITableView(frame: .zero, style: .plain).then {
        $0.backgroundColor = .custom(.gray_f6f8fd)
        $0.separatorStyle = .none
        $0.rowHeight = UITableView.automaticDimension
        $0.delegate = self
        $0.dataSource = self
        $0.tableHeaderView = header

    }
    
    private lazy var nameCell = ValueDetailCell().then {
        $0.title.text = "设备名称".localizedString
        $0.valueLabel.text = " "
    }
    
    private lazy var deleteButton = ImageTitleButton(frame: .zero, icon: nil, title: "删除设备".localizedString, titleColor: UIColor.custom(.black_333333), backgroundColor: UIColor.custom(.white_ffffff)).then {
        $0.clickCallBack = { [weak self] in
            self?.tipsAlert = TipsAlertView.show(message: "确定要删除吗?".localizedString, sureCallback: { [weak self] in
                self?.deleteDevice()
            }, removeWithSure: false)
        }
    }
    
    private var changeNameAlert: InputAlertView?
    
    private var tipsAlert: TipsAlertView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func setupViews() {
        view.backgroundColor = .custom(.gray_f6f8fd)
        view.addSubview(tableView)
        view.addSubview(deleteButton)
    }
    
    override func setupConstraints() {
        deleteButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(15)
            $0.right.equalToSuperview().offset(-15)
            $0.height.equalTo(50)
            $0.bottom.equalToSuperview().offset(-10 - Screen.bottomSafeAreaHeight)
        }
        
        tableView.snp.makeConstraints {
            $0.top.left.right.equalToSuperview()
            $0.bottom.equalTo(deleteButton.snp.top).offset(-10)
        }
    }
    
    
}

extension PrivateDeviceDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cells[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if cells[indexPath.row].title.text == "设备名称".localizedString {
            changeNameAlert = InputAlertView(labelText: "设备名称".localizedString, placeHolder: "请输入设备名称".localizedString, saveCallback: { [weak self] (text) in
                guard let self = self else { return }
                self.changeDeviceName(name: text)
            })
            if nameCell.valueLabel.text != " " {
                changeNameAlert?.textField.text = nameCell.valueLabel.text
            }
            SceneDelegate.shared.window?.addSubview(changeNameAlert!)
        }
    }
    
}

extension PrivateDeviceDetailViewController {

    private func deleteDevice() {
        guard let device = device else {
            showToast(string: "error")
            return
        }
        DoorLockUtil.deleteDoorLock(lock_id: device.iid)
        tipsAlert?.removeFromSuperview()
        showToast(string: "删除成功".localizedString)
        navigationController?.popToRootViewController(animated: true)
    }
    
    private func changeDeviceName(name: String) {
        guard let device = device else {
            showToast(string: "error")
            return
        }
        device.name = name
        DoorLockUtil.editDoorLock(lock_id: device.iid, name: name)
        changeNameAlert?.removeFromSuperview()
        showToast(string: "保存成功".localizedString)
        header.deviceTypeLabel.text = name
        nameCell.valueLabel.text = name
        self.renameCallback?(name)
    }
    
    
}

