//
//  DeviceSettingViewController.swift
//  ZhiTing
//
//  Created by iMac on 2021/3/16.
//

import UIKit
import IQKeyboardManagerSwift

class DeviceSettingViewController: BaseViewController {
    var device_id: Int?
    var device: Device?
    var area = Area()
    
    /// 级联更新子设备位置(网关时传true)
    var is_gateway: Bool?

    var plugin_url = ""
    private lazy var header = DeviceSettingHeader()

    private lazy var deviceAreaSettingView = DeviceLocationSettingView()
    
    private lazy var doneButton = ImageTitleButton(frame: .zero, icon: nil, title: "完成".localizedString, titleColor: .custom(.white_ffffff), backgroundColor: .custom(.blue_2da3f6)).then {
        $0.clickCallBack = { [weak self] in
            guard let self = self else { return }
            self.done()
        }
    }
    
    /// 是否设置为常用设备
    private lazy var selectionButton = SelectButton(type: .rounded)
    
    private lazy var selectionLabel = Label().then {
        $0.font = .font(size: 14, type: .medium)
        $0.text = "设置为常用设备".localizedString
        $0.textColor = .custom(.black_333333)
        $0.isUserInteractionEnabled = true
        $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapSelection)))
    }

    private var addAreaAlertView: InputAlertView?

    deinit {
        if deviceAreaSettingView.collectionView.observationInfo != nil {
            deviceAreaSettingView.collectionView.removeObserver(self, forKeyPath: "contentSize")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        deviceAreaSettingView.collectionView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "设置".localizedString
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.enableAutoToolbar = false
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        requestNetwork()
    }
    
    @objc private func tapSelection() {
        selectionButton.isSelected = !selectionButton.isSelected
    }
    
    override func setupViews() {
        view.addSubview(header)
        view.addSubview(deviceAreaSettingView)
        view.addSubview(selectionButton)
        view.addSubview(selectionLabel)
        view.addSubview(doneButton)
        
        if self.area.areaType == .family {
            header.addAreaButton.setTitle("添加房间".localizedString, for: .normal)
        } else {
            header.addAreaButton.setTitle("添加部门".localizedString, for: .normal)
        }

        header.addAreaButton.clickCallBack = { [weak self] _ in
            guard let self = self else { return }
            let text1 = self.area.areaType == .family ? "房间名称".localizedString : "部门名称".localizedString
            let text2 = self.area.areaType == .family ? "请输入房间名称".localizedString : "请输入部门名称".localizedString

            let addAreaAlertView = InputAlertView(labelText: text1, placeHolder: text2) { [weak self] text in
                guard let self = self else { return }
                self.addLocation(name: text)
            }
            
            self.addAreaAlertView = addAreaAlertView
            SceneDelegate.shared.window?.addSubview(addAreaAlertView)
        }
    }
    
    override func setupConstraints() {
        header.snp.makeConstraints {
            $0.right.left.equalToSuperview()
            $0.top.equalToSuperview().offset(Screen.k_nav_height)
        }
        
        
        doneButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-10 - Screen.bottomSafeAreaHeight)
            $0.left.equalToSuperview().offset(15)
            $0.right.equalToSuperview().offset(-15)
            $0.height.equalTo(50)
        }
        
        selectionButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(15)
            $0.height.width.equalTo(18.5)
            $0.top.equalTo(deviceAreaSettingView.snp.bottom).offset(10)
        }
        
        selectionLabel.snp.makeConstraints {
            $0.centerY.equalTo(selectionButton.snp.centerY)
            $0.left.equalTo(selectionButton.snp.right).offset(15)
        }

        deviceAreaSettingView.snp.makeConstraints {
            $0.top.equalTo(header.snp.bottom)
            $0.left.equalToSuperview().offset(15)
            $0.right.equalToSuperview().offset(-15)
            $0.height.equalTo(200)
        }
        
        view.layoutIfNeeded()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            guard let height = (change![.newKey] as? CGSize)?.height  else {
                return
            }
            
            if height > view.bounds.height - Screen.k_nav_height - self.header.frame.height - 60 - Screen.bottomSafeAreaHeight - 40 {
                deviceAreaSettingView.snp.remakeConstraints {
                    $0.top.equalTo(header.snp.bottom)
                    $0.left.equalToSuperview().offset(15)
                    $0.right.equalToSuperview().offset(-15)
                    $0.height.equalTo(view.bounds.height - self.header.frame.height - 60 - Screen.bottomSafeAreaHeight - 40 - Screen.k_nav_height)
                }
            } else {
                deviceAreaSettingView.snp.remakeConstraints {
                    $0.top.equalTo(header.snp.bottom)
                    $0.left.equalToSuperview().offset(15)
                    $0.right.equalToSuperview().offset(-15)
                    $0.height.equalTo(height)
                }
            }
            
        }
    }

}

extension DeviceSettingViewController {
    private func requestNetwork() {
        guard let device_id = device_id else {
            return
        }

        ApiServiceManager.shared.deviceDetail(area: area, device_id: device_id) { [weak self] (response) in
            guard let self = self else { return }
            self.device = response.device_info
            self.device?.plugin_id = response.device_info.plugin?.id ?? ""
            if self.header.deviceNameTextField.text == "" {
                self.header.deviceNameTextField.text = response.device_info.name
            }
            self.getLocationList()

        } failureCallback: { [weak self] (statusCode, errMessage) in
            self?.showToast(string: errMessage)
        }

    }
    
    private func getLocationList() {
        if area.areaType == .family {
            ApiServiceManager.shared.areaLocationsList(area: area) { [weak self] response in
                guard let self = self else { return }
                self.deviceAreaSettingView.selected_location_id = self.device?.location?.id ?? -1
                self.deviceAreaSettingView.locations = response.locations
                
            } failureCallback: { code, err in
                
            }
        } else {
            ApiServiceManager.shared.departmentList(area: area) { [weak self] response in
                guard let self = self else { return }
                self.deviceAreaSettingView.selected_location_id = self.device?.department?.id ?? -1
                self.deviceAreaSettingView.locations = response.departments
                
            } failureCallback: { code, err in
                
            }
        }
    }
    
    private func addLocation(name: String) {
        
        if deviceAreaSettingView.locations.map(\.name).contains(name) {
            let text = getCurrentLanguage() == .chinese ? "\(name)已存在" : "\(name) already existed"
            self.showToast(string: text)
            return
        }
        
        addAreaAlertView?.saveButton.selectedChangeView(isLoading: true)
        if area.areaType == .family {
            //添加房间
            ApiServiceManager.shared.addLocation(area: area, name: name) { [weak self] (response) in
                self?.addAreaAlertView?.saveButton.selectedChangeView(isLoading: false)
                self?.getLocationList()
                self?.addAreaAlertView?.removeFromSuperview()
            } failureCallback: { [weak self] code, err in
                self?.showToast(string: err)
                self?.addAreaAlertView?.saveButton.selectedChangeView(isLoading: true)
            }
        } else {
            //添加部门
            ApiServiceManager.shared.addDepartment(area: area, name: name) { [weak self] (response) in
                self?.addAreaAlertView?.saveButton.selectedChangeView(isLoading: false)
                self?.getLocationList()
                self?.addAreaAlertView?.removeFromSuperview()
            } failureCallback: { [weak self] code, err in
                self?.showToast(string: err)
                self?.addAreaAlertView?.saveButton.selectedChangeView(isLoading: true)
            }
        }
        
        
    }
    
    
    private func done() {
        
            guard
                let device_id = device?.id,
                let name = header.deviceNameTextField.text
            else {
                showToast(string: "error")
                return
                
            }
            
            if header.deviceNameTextField.text == "" || header.deviceNameTextField.text?.replacingOccurrences(of: " ", with: "") == "" {
                showToast(string: "设备名不能为空".localizedString)
                return
            }
            
            

            /// edit device
            ApiServiceManager.shared.editDevice(area: area, device_id: device_id, name: name, location_id: deviceAreaSettingView.selected_location_id, common: selectionButton.isSelected, cascade_location: is_gateway) { [weak self] _ in
                guard let self = self else { return }
                if self.device?.model == "IP Camera" || self.device?.model == "MH-IC2MDCMW001W" {//无插件信息,则为摄像头
                    #if !(targetEnvironment(simulator))
                    let vc = CameraViewController()
                    vc.deviceID = self.device_id
                    self.navigationController?.pushViewController(vc, animated: true)
                    #endif
                }else{
                    let device_id = self.device?.id
                    let vc = DeviceWebViewController(link: self.plugin_url, device_id: device_id)
                    vc.area = self.area
                    self.navigationController?.pushViewController(vc, animated: true)
                }

                if let count = self.navigationController?.viewControllers.count,
                   count - 2 > 0,
                   var vcs = self.navigationController?.viewControllers {
                    vcs.remove(at: count - 2)
                    self.navigationController?.viewControllers = vcs
                }
                
                
            } failureCallback: { [weak self] (code, err) in
                self?.showToast(string: err)
            }
        
    }
}

