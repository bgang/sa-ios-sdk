//
//  ResetProEditionCell.swift
//  ZhiTing
//
//  Created by iMac on 2022/7/4.
//


import UIKit

class ResetProEditionCell: UITableViewCell, ReusableView {

    private lazy var titleLabel = UILabel().then {
        $0.font = .font(size: 14, type: .bold)
        $0.textColor = .custom(.black_3f4663)
        $0.text = "重置专业版密码".localizedString
        
    }

    private lazy var detailLabel = UILabel().then {
        $0.font = .font(size: 12, type: .regular)
        $0.textColor = .custom(.gray_94a5be)
        $0.text = "开启后，所有成员都可以无需旧密码即可设置专业版的新密码，退出维护模式自动关闭".localizedString
        $0.numberOfLines = 0
    }
    
    lazy var switchBtn = SettingSwitchButton(frame: CGRect(x: 0, y: 0, width: 40, height: 20))

    private lazy var lineBottom = UIView().then {
        $0.backgroundColor = .custom(.gray_eeeeee)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        selectionStyle = .none
        contentView.backgroundColor = .custom(.white_ffffff)
        contentView.addSubview(titleLabel)
        contentView.addSubview(switchBtn)
        contentView.addSubview(detailLabel)
        contentView.addSubview(lineBottom)
    }
    
    private func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(20)
            $0.left.equalToSuperview().offset(15)
            $0.width.equalTo(100)
        }
        
        switchBtn.snp.makeConstraints {
            $0.centerY.equalTo(titleLabel.snp.centerY)
            $0.right.equalToSuperview().offset(-15)
            $0.height.equalTo(18)
            $0.width.equalTo(36)
        }
        

        detailLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(15)
            $0.left.equalToSuperview().offset(15)
            $0.right.equalToSuperview().offset(-15)
        }

        lineBottom.snp.makeConstraints {
            $0.top.equalTo(detailLabel.snp.bottom).offset(15)
            $0.left.right.equalToSuperview()
            $0.height.equalTo(0.5)
            $0.bottom.equalToSuperview()
        }

    }

    
}
