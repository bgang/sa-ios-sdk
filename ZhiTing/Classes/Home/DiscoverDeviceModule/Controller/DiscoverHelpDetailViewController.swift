//
//  DiscoverHelpDetailViewController.swift
//  ZhiTing
//
//  Created by 黄佳玮 on 2022/8/3.
//

import Foundation
import SwiftUI

class DiscoverHelpDetailViewController: BaseViewController {
    
    private lazy var titleLabel = UILabel().then {
        $0.font = .font(size: 14, type: .bold)
        $0.textColor = .custom(.black_3f4663)
        $0.text = "无法扫描到设备怎么办？".localizedString
    }
    
    private lazy var detailLabel = UILabel().then {
        $0.font = .font(size: 14, type: .regular)
        $0.textColor = .custom(.black_3f4663)
        $0.numberOfLines = 0
        $0.text = "设备正常连通电源且设备已完成重置的情况下，若无法扫描到对应设备， 请确认您已在【我的-支持品牌-品牌详情】中添加对应支持插件，若依旧无法扫描到对应设备，请您根据以下指导对应进行操作：".localizedString
    }
    
    private lazy var scrollView = UIScrollView().then {
        $0.showsVerticalScrollIndicator = false
        $0.isDirectionalLockEnabled = true
        $0.alwaysBounceVertical = false
    }
    
    private lazy var contentView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "添加帮助".localizedString
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func setupViews() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(detailLabel)
        setupContent()
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.left.right.equalToSuperview().offset(0)
            $0.top.equalToSuperview().offset(Screen.k_nav_height + 10)
            $0.bottom.equalTo(self.view).offset(-Screen.bottomSafeAreaHeight)
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ZTScaleValue(15))
            $0.top.equalToSuperview().offset(0)
            $0.height.equalTo(14)
        }
        
        detailLabel.snp.makeConstraints {
            $0.left.equalTo(titleLabel)
            $0.top.equalTo(titleLabel.snp.bottom).offset(ZTScaleValue(15.5))
            $0.right.equalToSuperview().offset(-17)
            $0.height.equalTo(ZTScaleValue(87))
        }
    }
    
    private func setupContent() {
        
        for index in 0...3 {
            let titleLabel = UILabel().then {
                $0.font = .font(size: 14, type: .bold)
                $0.textColor = .custom(.black_3f4663)
            }
            
            let detailLabel = UILabel().then {
                $0.font = .font(size: 14, type: .regular)
                $0.textColor = .custom(.black_3f4663)
                $0.numberOfLines = 0
            }
            let roundView = UIView().then {
                $0.layer.masksToBounds = true
                $0.layer.cornerRadius = 5.5
                $0.backgroundColor = .custom(.black_3f4663)
                $0.alpha = 0.2
            }
            contentView.addSubview(roundView)
            contentView.addSubview(titleLabel)
            contentView.addSubview(detailLabel)
            
            switch index{
            case 0:
                titleLabel.text = "1. Broadlink"
                detailLabel.text = "请在Broadlink应用程序中点击进入设备详情，在设备属性页面内设置关闭设备上锁功能。"
                titleLabel.snp.makeConstraints {
                    $0.left.equalTo(self.titleLabel)
                    $0.top.equalTo(self.detailLabel.snp.bottom).offset(ZTScaleValue(27))
                    $0.height.equalTo(14)
                }
                roundView.snp.makeConstraints {
                    $0.top.equalTo(titleLabel).offset(ZTScaleValue(-2.5))
                    $0.size.equalTo(CGSize(width: 11, height: 11))
                    $0.left.equalToSuperview().offset(ZTScaleValue(11.5))
                }
                detailLabel.snp.makeConstraints {
                    $0.left.right.equalTo(self.detailLabel)
                    $0.top.equalTo(titleLabel.snp.bottom).offset(ZTScaleValue(12))
                    $0.height.equalTo(ZTScaleValue(40))
                }
            case 1:
                titleLabel.text = "2. Yeelight"
                detailLabel.text = "请在 Yeelight  APP中设置该设备允许被发现。"
                titleLabel.snp.makeConstraints {
                    $0.left.equalTo(self.titleLabel)
                    $0.top.equalTo(self.detailLabel.snp.bottom).offset(ZTScaleValue(111))
                    $0.height.equalTo(16)
                }
                roundView.snp.makeConstraints {
                    $0.top.equalTo(titleLabel).offset(ZTScaleValue(-2.5))
                    $0.size.equalTo(CGSize(width: 11, height: 11))
                    $0.left.equalToSuperview().offset(ZTScaleValue(11.5))
                }
                detailLabel.snp.makeConstraints {
                    $0.left.right.equalTo(self.detailLabel)
                    $0.top.equalTo(titleLabel.snp.bottom).offset(ZTScaleValue(12))
                    $0.height.equalTo(ZTScaleValue(16))
                }
            case 2:
                titleLabel.text = "3. Homekit"
                detailLabel.text = "请确保设备未被其他中心枢纽连接，若已连接其他中心枢纽，请先进行解绑。"
                titleLabel.snp.makeConstraints {
                    $0.left.equalTo(self.titleLabel)
                    $0.top.equalTo(self.detailLabel.snp.bottom).offset(ZTScaleValue(173))
                    $0.height.equalTo(14)
                }
                roundView.snp.makeConstraints {
                    $0.top.equalTo(titleLabel).offset(ZTScaleValue(-2.5))
                    $0.size.equalTo(CGSize(width: 11, height: 11))
                    $0.left.equalToSuperview().offset(ZTScaleValue(11.5))
                }
                detailLabel.snp.makeConstraints {
                    $0.left.right.equalTo(self.detailLabel)
                    $0.top.equalTo(titleLabel.snp.bottom).offset(ZTScaleValue(12))
                    $0.height.equalTo(ZTScaleValue(40))
                }
            case 3:
                titleLabel.text = "4. 小米"
                detailLabel.text = "添加您的小米品牌设备时，若扫描不到设备，请检查您是否在【我的-支持品牌-小米插件包-插件详情】内登录了您的小米账号（您所要添加的设备需要先在米家应用程序内添加到您的账号设备列表下），同时请您检查已添加的小米账号当前登录状态，账号在正常登录状态下，设备才可正常添加使用；\n\n如您在使用过程中遇到问题，也可在智汀家庭云app”我的--问题反馈“提交反馈，我们将及时响应解决，同时也欢迎您提出宝贵的建议。"
                titleLabel.snp.makeConstraints {
                    $0.left.equalTo(self.titleLabel)
                    $0.top.equalTo(self.detailLabel.snp.bottom).offset(ZTScaleValue(257))
                    $0.height.equalTo(14)
                }
                roundView.snp.makeConstraints {
                    $0.top.equalTo(titleLabel).offset(ZTScaleValue(-2.5))
                    $0.size.equalTo(CGSize(width: 11, height: 11))
                    $0.left.equalToSuperview().offset(ZTScaleValue(11.5))
                }
                detailLabel.snp.makeConstraints {
                    $0.left.right.equalTo(self.detailLabel)
                    $0.top.equalTo(titleLabel.snp.bottom).offset(ZTScaleValue(12))
                    $0.height.equalTo(ZTScaleValue(205))
                    $0.bottom.equalToSuperview()
                }
            default:
                break
            }
        }
        
    }
    
}
