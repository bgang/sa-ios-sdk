//
//  ApiService+Header.swift
//  ZhiTing
//
//  Created by iMac on 2021/7/29.
//

import Foundation
import Moya

extension ApiService {
    var headers: [String : String]? {
        var headers = [String : String]()
        headers["Content-Type"] = "application/json"
        headers["traceparent"] = generateTraceParentId()
        
        switch self {
        case .checkPluginUpdate(_, let area),
                .brands(_, let area),
                .brandDetail(_, let area),
                .plugins(let area, _),
                .deletePluginById(let area, _),
                .pluginDetail(_, let area),
                .installPlugin(let area, _, _),
                .deletePlugin(let area, _, _),
                .areaDetail(let area),
                .changeAreaName(let area, _),
                .deleteArea(let area, _),
                .quitArea(let area),
                .getInviteQRCode(let area, _, _),
                .memberList(let area),
                .userDetail(let area, _),
                .deleteMember(let area, _),
                .editMember(let area, _, _, _),
                .rolesList(let area),
                .rolesPermissions(let area, _),
                .locationsList(let area),
                .locationDetail(let area, _),
                .addLocation(let area, _),
                .changeLocationName(let area, _, _),
                .deleteLocation(let area, _),
                .setLocationOrders(let area, _),
                .deviceList(_, _, let area),
                .deviceDetail(let area, _, _),
                .allDevicesSorting(let area, _),
                .locationDevicesSorting(let area, _, _),
                .departmentDevicesSorting(let area, _, _),
                .deleteDevice(let area, _),
                .editDevice(let area, _, _, _, _, _, _, _),
                .userCommonDeviceList(let area),
                .updateUserCommonDeviceList(let area, _),
                .deviceLogoList(let area, _),
                .sceneList(_, let area),
                .sceneDetail(_, let area),
                .sceneLogs(_, _, let area),
                .sceneExecute(_, _, let area),
                .deleteScene(_, let area),
                .createScene(_, let area),
                .editScene(_, _, let area),
                .scopeToken(let area, _),
                .transferOwner(let area, _),
                .editSAUser(let area, _, _, _, _, _, _),
                .deleteSA(let area, _, _, _, _),
                .scopeList(area: let area),
                .temporaryIP(let area, _),
                .getSAToken(let area),
                .getCaptcha(let area),
                .settingTokenAuth(let area, _),
                .getSAExtensions(let area),
                .getSoftwareVersion(let area),
                .getSoftwareLatestVersion(let area),
                .updateSoftware(let area, _),
                .getFirmwareLatestVersion(let area),
                .getFirmwareVersion(let area),
                .updateFirmware(let area, _),
               .migrationAddr(let area),
               .migrationCloudToLocal(let area, _, _, _),
               .commonDeviceMajorList(let area),
               .commonDeviceMinorList(let area, _),
               .departmentList(let area),
               .addDepartment(let area, _),
               .departmentDetail(let area, _),
               .addDepartmentMember(let area, _, _),
               .updateDepartment(let area, _, _, _),
               .deleteDepartment(let area, _),
               .setDepartmentOrders(let area, _),
               .changePWD(let area, _, _),
               .saUploadFile(let area, _, _),
               .unbindThirdPartyCloud(let area, _),
               .thirdPartyCloudListSA(let area),
               .addCameraDevice(let area, _, _, _, _, _, _, _),
               .associatePrivateDeviceToGateWay(let area, _, _, _, _, _),
               .getDeviceMqttPassword(let area, _, _),
               .exitMaintenance(let area),
               .changeOwner(let area),
               .resetProPwd(let area, _),
               .getMaintenanceToken(let area):
            if let areaId = area.id {
                headers["Area-ID"] = "\(areaId)"
            }
            if let saId = area.sa_id {
                headers["current_sa_id"] = "\(saId)"
            }
            headers["smart-assistant-token"] = area.sa_user_token
            
        case .commonDeviceList,
                .cloudUserDetail,
                .register,
                .login,
                .logout,
                .captcha,
                .unregister,
                .unregisterList,
                .editCloudUser,
                .checkSABindState,
                .addSADevice,
                .defaultLocationList,
                .areaList,
                .createArea,
                .getDeviceAccessToken,
                .downloadPlugin,
                .forgetPwd,
                .getAppVersions,
                .getAppSupportApiVersion,
                .getSAStatus,
                .thirdPartyCloudListSC,
                .getSASupportApiVersion,
                .feedbackList,
                .feedbackDetail,
                .createFeedback,
                .checkEmailIsBind,
                .sendEmailCaptcha,
                .editEmail,
                .getSocks5Auth:
            break
            
        case .bindCloud(let area, _, _, let saId, _):
            if let saId = saId {
                headers["SA-ID"] = "\(saId)"
            }
            if let saId = area.sa_id {
                headers["current_sa_id"] = "\(saId)"
            }
            
            headers["smart-assistant-token"] = area.sa_user_token
            
        case .syncArea(_, _, let token):
            headers["smart-assistant-token"] = token
            
        case .scanQRCode(_, _, _, _, let token):
            if let token = token {
                headers["smart-assistant-token"] = token
            }
            
        case .temporaryIPBySAID(let saID, _):
            headers["SA-ID"] = saID
            
        case .scUploadFile:
            headers["Content-Type"] = "multipart/form-data"
            
        case .setSceneSort(let area, _):
            headers["smart-assistant-token"] = area.sa_user_token
            if let saId = area.sa_id {
                headers["current_sa_id"] = "\(saId)"
            }
            
        case .deleteAreaWithNoToken:
            headers["Content-Type"] = "application/x-www-form-urlencoded"

  

        }
        return headers
        
    }
}



fileprivate func generateTraceParentId() -> String {
    func randomHexString(length: Int) -> String {
      let letters = "0123456789abcdef"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    return "00-" + randomHexString(length: 32) + "-" + randomHexString(length: 16) + "-01"
}
