//
//  ApiService+Method.swift
//  ZhiTing
//
//  Created by iMac on 2021/7/29.
//

import Foundation
import Moya



extension ApiService {
    var method: Moya.Method {
        switch self {

        case .editCloudUser,
                .changeAreaName,
                .changeLocationName,
                .setLocationOrders,
                .editDevice,
                .editScene,
                .editMember,
                .editSAUser,
                .transferOwner,
                .updateDepartment,
                .setDepartmentOrders,
                .changePWD,
                .setSceneSort,
                .allDevicesSorting,
                .locationDevicesSorting,
                .departmentDevicesSorting,
                .updateUserCommonDeviceList,
                .editEmail,
                .settingTokenAuth,
                .changeOwner:
            return .put
            
        case .cloudUserDetail,
                .brands,
                .brandDetail,
                .plugins,
                .pluginDetail,
                .defaultLocationList,
                .areaList,
                .areaDetail,
                .locationDetail,
                .deviceDetail,
                .locationsList,
                .deviceList,
                .sceneList,
                .sceneDetail,
                .sceneLogs,
                .captcha,
                .userDetail,
                .memberList,
                .rolesList,
                .rolesPermissions,
                .checkSABindState,
                .scopeList,
                .temporaryIP,
                .temporaryIPBySAID,
                .getSAToken,
                .commonDeviceList,
                .checkPluginUpdate,
                .getSoftwareVersion,
                .getSoftwareLatestVersion,
                .getFirmwareLatestVersion,
                .getFirmwareVersion,
                .commonDeviceMajorList,
                .commonDeviceMinorList,
                .departmentList,
                .departmentDetail,
                .migrationAddr,
                .getSAExtensions,
                .unregisterList,
                .getAppVersions,
                .deviceLogoList,
                .getAppSupportApiVersion,
                .getSAStatus,
                .thirdPartyCloudListSC,
                .thirdPartyCloudListSA,
                .getSASupportApiVersion,
                .feedbackList,
                .feedbackDetail,
                .userCommonDeviceList,
                .checkEmailIsBind,
                .sendEmailCaptcha,
                .getDeviceMqttPassword,
                .getSocks5Auth,
                .downloadPlugin,
                .getMaintenanceToken:

            return .get
            
        case .deletePluginById,
                .deletePlugin,
                .deleteArea,
                .deleteLocation,
                .deleteDevice,
                .deleteScene,
                .deleteDepartment,
                .deleteMember,
                .unregister,
                .deleteSA,
                .unbindThirdPartyCloud,
                .deleteAreaWithNoToken,
                .quitArea,
                .exitMaintenance:
            return .delete
        
        case .installPlugin,
                .createArea,
                .addLocation,
                .getDeviceAccessToken,
                .createScene,
                .sceneExecute,
                .register,
                .login,
                .logout,
                .getInviteQRCode,
                .scanQRCode,
                .syncArea,
                .addSADevice,
                .bindCloud,
                .scopeToken,
                .getCaptcha,
                .updateFirmware,
                .addDepartment,
                .migrationCloudToLocal,
                .scUploadFile,
                .saUploadFile,
                .addDepartmentMember,
                .forgetPwd,
                .createFeedback,
                .addCameraDevice,
                .associatePrivateDeviceToGateWay,
                .updateSoftware,
                .resetProPwd:
            return .post
       
        }
    }
}
