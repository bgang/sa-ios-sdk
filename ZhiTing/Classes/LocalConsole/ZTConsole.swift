//
//  ZTLog.swift
//  ZhiTing
//
//  Created by iMac on 2022/7/18.
//

import Foundation


struct ZTConsole {
    
    static func log(_ items: Any..., separator: String = " ", terminator: String = "\n") {
        print(items, separator: separator, terminator: terminator)
        if #available(iOS 14.0, *) {
            if LCManager.shared.isVisible {
                LCManager.shared.print(items)
            }
        }
    }

}
